<?php

class MY_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('authlib');
    }

    //Create the header of the pages
    public function createHeader() {
        $user = $this->authlib->is_loggedin();
        $useremail['isLogged'] = $user;
        $id = $this->UserModel->getUserID($user);
        $result = (Array) $id;

        if ($user == false) {
            $useremail['isLogged'] = "no";
        } else {
            $useremail['id'] = $result['id'];
        }
        $this->load->view('headerview', $useremail);
    }

    //Create the footer of the pages
    public function createFooter() {
        $this->load->view('footerView');
    }

    //Create view with no data passing
    public function createView($mainView) {
        $this->createHeader();

        $this->load->view($mainView);
        $this->createFooter();
    }
    //Create view passing a string
    public function createViewWithString($mainView, $data) {
        $msg['message'] = $data;

        $this->createHeader();
        $this->load->view($mainView, $msg);
        $this->createFooter();
        return;
    }
    //Create view passing an array
    public function createViewWithArray($mainView, $data) {

        $subjects = (array) $data;
        $this->createHeader();
        $subjects['subject'] = $data;

        $this->load->view($mainView, $subjects);

        $this->createFooter();
    }
    //Create view passing a string and array
    public function createViewWithArraynStr($mainView, $data, $String) {

        $subjects = (array) $data;
        $this->createHeader();
        $subjects['subject'] = $data;
        $subjects['searchTerm'] = $String;
        $this->load->view($mainView, $subjects);

        $this->createFooter();
    }
}

?>
