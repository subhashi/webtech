<?php

class QuestionController extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->ci = &get_instance();
        $this->ci->load->model('QuestionModel');
    }

    //View the questions page
    public function questionView() {
        $this->createView("questionsview");
    }

    //View the ask questions page
    public function askQuestionView() {
        $user = $this->authlib->is_loggedin();

        if ($user !== false) {
            $userGroup = $this->authlib->checkUserGroup($user);
            $result = (Array) $userGroup;
            if ($result['userGroup'] == '3') //If the user is an admin he is not alloed to ask questions and an error page will be displayed
                $userGroup = $this->createViewWithString("errorview", "Admin users are not allowed to ask questions.");
            else
                $this->createViewWithArray("askquestionview", $this->showSubjects());
        }
        else {
            $this->createViewWithArray("askquestionview", $this->showSubjects());
        }
    }

    // View all the subjects
    function showSubjects() {        
        $query = $this->QuestionModel->getSubjects();
        return $query;
    }

    //When user clicks on the title of a question this page will be displayed
    public function selectedQuesView($question) {

        //Get user id and send it to the page to be used later in certain functions
        $user = $this->authlib->is_loggedin();

        $data['grp'] = '';

        //Get user group of the user and send it to the page to be used later in certain functions
        if ($user !== false) {
            $userGroup = $this->authlib->checkUserGroup($user);
            $result = (Array) $userGroup;
            $data['grp'] = $result['userGroup'];
        }
        
        $data['question'] = $question;
        $this->createViewWithArray("selectedques", $data);
    }

    //Load the slected question
    public function loadSelectedQues() {

        $question = $this->input->get('questionId');
        $this->selectedQuesView($question);
    }
}

?>
