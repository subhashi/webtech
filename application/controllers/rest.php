<?php

class Rest extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('UserModel');
        $this->load->model('QuestionModel');
        $this->load->model('AnswerModel');
        $this->load->model('VoteModel');
        $this->load->model('SearchModel');
        $this->load->library('authlib');
    }

    //Map all the get and post requests
    public function _remap() {
        $request_method = $this->input->server('REQUEST_METHOD');
        switch (strtolower($request_method)) {
            case 'get' : $this->get();
                break;
            case 'post' : $this->post();
                break;
            default:
                show_error('Unsupported method', 404); // CI function for 404 errors
                break;
        }
    }

    public function get() {
        $args = $this->uri->uri_to_assoc(1);
        switch ($args['rest']) {
            //Basic search
            case 'search' :
                $searchTerm = $this->input->get('searchTerm');
                $searchTerm = str_replace("%20", " ", $searchTerm); //Replace the%20 with spaces and crete and array of search terms
                $pageNumber = $args['pg'];

                $numOfPage = $this->SearchModel->numOfPage($searchTerm); //Get number of pages to view according to search results
                $response["numOfPage"] = $numOfPage;

                $res = $this->SearchModel->basicSearch($searchTerm, $pageNumber); //Ger the questions from basic search
                $response["questions"] = $res;

                if ($res !== FALSE) {
                    $data = $this->QuestionModel->getSubjects(); // if there are search results thn get subjects for each question
                    $response["subjects"] = $data;

                    for ($i = 0; $i < count($res); $i++) {
                        $result = (Array) $res[$i];
                        $users[$i] = $result['askedBy'];
                    }

                    $askedUsers = $this->QuestionModel->getUsers($users); // if there are search results thn get users for each question
                    $response["users"] = $askedUsers;

                    for ($i = 0; $i < count($res); $i++) {
                        $result = (Array) $res[$i];
                        $tag[$i] = $result['tags'];
                    }

                    $tags = $this->QuestionModel->getTag($tag); // if there are search results thn get tags for each question
                    $response["tags"] = $tags;
                }
                echo json_encode($response); //Send the results as a Jason encoded
                break;
            //Advanced search
            case 'advancesearch':
                //Get all the filters and add them into an array
                $searchTerms = array();
                $searchTermTitle = $this->input->get('advancesearchTermTitle');
                $searchTermDesc = $this->input->get('advancesearchTermDesc');
                $searchTerms['searchTermTitle'] = str_replace("%20", " ", $searchTermTitle);
                $searchTerms['searchTermDesc'] = str_replace("%20", " ", $searchTermDesc);
                $searchTerms['pageNumber'] = $args['pg'];

                $searchTerms['subject'] = $this->input->get('selectedSubject');
                $searchTerms['tag'] = $this->input->get('tag');
                $searchTerms['sortBy'] = $this->input->get('sortBy');
                $searchTerms['ordertBy'] = $this->input->get('ordertBy');
                $searchTerms['askedBy'] = $this->input->get('askedBy');
                $searchTerms['type'] = $this->input->get('type');

                //Get the number of pages according to search results
                $numOfPage = $this->SearchModel->numOfPageAd($searchTerms, RETURN_NUM_OF_PAGES);
                $response["numOfPage"] = $numOfPage;

                //Get questions
                $res = $this->SearchModel->advanceSearch($searchTerms);
                $response["questions"] = $res;

                if ($res !== FALSE) {
                    $data = $this->QuestionModel->getSubjects(); // if there are search results thn get subjects for each question
                    $response["subjects"] = $data;

                    for ($i = 0; $i < count($res); $i++) {
                        $result = (Array) $res[$i];
                        $users[$i] = $result['askedBy'];
                    }

                    $askedUsers = $this->QuestionModel->getUsers($users); // if there are search results thn get users for each question
                    $response["users"] = $askedUsers;

                    $tag = array();
                    for ($i = 0; $i < count($res); $i++) {
                        $result = (Array) $res[$i];
                        $tag[$i] = $result['tags'];
                    }

                    $tags = $this->QuestionModel->getTag($tag); // if there are search results thn get tags for each question
                    $response["tags"] = $tags;
                }
                echo json_encode($response); //Send the results as a Jason encoded

                break;
            //Get questions for 'Questions' page
            case 'getquestion':

                $numOfPage = $this->QuestionModel->numOfPages(); //Get number of pages according to the total number of questions
                $response["numOfPage"] = $numOfPage;

                $pageNumber = $args['pg'];
                $res = $this->QuestionModel->loadQuestions($pageNumber); //Get questions for each page
                $response["questions"] = $res;

                $data = $this->QuestionModel->getSubjects(); //Get subjects' names for wach question
                $response["subjects"] = $data;

                for ($i = 0; $i < count($res); $i++) {
                    $result = (Array) $res[$i];
                    $users[$i] = $result['askedBy'];
                }

                $askedUsers = $this->QuestionModel->getUsers($users); //Get user names for each question
                $response["users"] = $askedUsers;

                for ($i = 0; $i < count($res); $i++) {
                    $result = (Array) $res[$i];
                    $tag[$i] = $result['tags'];
                }

                $tags = $this->QuestionModel->getTag($tag); //Get tags for each question
                $response["tags"] = $tags;

                echo json_encode($response); //Send the results as a Jason encoded

                break;
            //Get selected question once user clicks on a title of a question
            case 'getselquestion':

                $res = $this->QuestionModel->loadSelQuestion($args); //Get number of pages according to the total number of questions
                $response["questions"] = $res;

                $result = (Array) $res[0];

                if ($result['answers'] != "") {
                    $answers = $this->QuestionModel->getAnswers($result['answers']); //Get the ansers of the seleced question
                    $response ["answers"] = $answers;

                    for ($i = 0; $i < count($answers); $i++) {
                        $resultsans = (Array) $answers[$i];
                        $usersans[$i] = $resultsans['answeredBy'];
                    }

                    $answeredUsers = $this->QuestionModel->getUsers($usersans); //Get the users for the answers
                    $response["usersans"] = $answeredUsers;
                } else {
                    $response ["answers"] = $result['answers'];
                }

                $data = $this->QuestionModel->getSubjects(); //Get the subjects for answers
                $response["subjects"] = $data;

                for ($i = 0; $i < count($res); $i++) {
                    $result = (Array) $res[$i];
                    $tag[$i] = $result['tags'];
                }

                $tags = $this->QuestionModel->getTag($tag); //Get the tags
                $response["tags"] = $tags;

                for ($i = 0; $i < count($res); $i++) {
                    $results = (Array) $res[$i];
                    $users[$i] = $results['askedBy'];
                }

                $askedUsers = $this->QuestionModel->getUsers($users); //Get the user name of the question asked user
                $response["users"] = $askedUsers;

                echo json_encode($response); //Send the results as a Jason encoded
                break;

            // Get the user for 'my profile' page
            case 'user':
                $res = $this->UserModel->getUserDetails($args);
                $response["users"] = $res;

                echo json_encode($response); //Send the results as a Jason encoded
                break;

            //Get the subjects for 'subjects' page
            case'subjects':
                $subjects = $this->QuestionModel->getSubjects();
                $response["subjects"] = $subjects;

                $subjectDesc = $this->QuestionModel->getSubjectDesc();
                $response["subjectDesc"] = $subjectDesc;

                echo json_encode($response); //Send the results as a Jason encoded
                break;

            //Get the users for 'tutors' ans 'students' pages
            case 'getusers':

                $pageNumber = $args['pg'];
                $userGroup = $this->input->get('type');

                $numOfPage = $this->UserModel->numOfPages($userGroup); //Get number of pages according to the number of users
                $response["numOfPage"] = $numOfPage;

                $res = $this->UserModel->getUsers($pageNumber, $userGroup); //Get users filtering from user groups
                $response["users"] = $res;

                $subjects = $this->QuestionModel->getSubjects(); //Get the subjects
                $response["subjects"] = $subjects;

                echo json_encode($response); //Send the results as a Jason encoded
                break;

            //Get the questions for 'my profile' and 'subjects' pages
            case 'searchspecific':
                $pageNumber = $args['pg'];
                $label = $args['label'];
                $label = str_replace("%20", " ", $label);
                $term = $args['term'];

                //Get the number of pages according to the number of records
                $numOfPage = $this->SearchModel->numOfPageSpecific($label, $term, RETURN_NUM_OF_PAGES, $pageNumber);
                $response["numOfPage"] = $numOfPage;

                //Get the results
                $res = $this->SearchModel->SearchSpecific($label, $term, $pageNumber);
                $response["results"] = $res;

                //Get the subjects
                $subjects = $this->QuestionModel->getSubjects();
                $response["subjects"] = $subjects;

                if (($label == 'question' || $label == 'subject') && $res !== false) {
                    for ($i = 0; $i < count($res); $i++) {
                        $result = (Array) $res[$i];
                        $users[$i] = $result['askedBy'];
                    }

                    //Get the users if there are results
                    $askedUsers = $this->QuestionModel->getUsers($users);
                    $response["users"] = $askedUsers;

                    for ($i = 0; $i < count($res); $i++) {
                        $result = (Array) $res[$i];
                        $tag[$i] = $result['tags'];
                    }

                    //Get the tags if there are results
                    $tags = $this->QuestionModel->getTag($tag);
                    $response["tags"] = $tags;
                }

                echo json_encode($response); //Send the results as a Jason encoded

                break;

            default:
                show_error('Unsupported resource-get', 404);
                break;
        }
    }

    public function post() {
        $args = $this->uri->uri_to_assoc(1);
        switch ($args['rest']) {
            //Authenticate user at sign in
            case 'authenticate' :
                $email = $this->input->post('email');
                $password = $this->input->post('password');
                $msg = $this->authlib->login($email, $password);

                if ($msg === true) {
                    $response["Status"] = "logged"; //If user is valid and can be signed in status is 'logged'                   
                } else {
                    $response["Status"] = $msg; //If user is not valid send the certain message                    
                }
                echo json_encode($response); //Send the results as a Jason encoded
                break;

            //Register the user
            case 'register' :
                //Get all the information that is necessary to register
                $name = $this->input->post('name');
                $email = $this->input->post('email');
                $password = $this->input->post('password');
                $conf_password = $this->input->post('conf_pword');
                $dob = $this->input->post('dateDob');
                $favouriteSubject = $this->input->post('selectedSubject');
                $userGroup = $this->input->post('userGroup');

                //Send user details to register
                $msg = $this->authlib->register($name, $email, $password, $conf_password, $dob, $favouriteSubject, $userGroup);

                if ($msg === true) {
                    $response["Status"] = "registered"; //Status is 'registered' if the user can be registered successfully                   
                } else {
                    $response["Status"] = $msg;
                }
                echo json_encode($response); //Send the results as a Jason encoded
                break;

            //Change the password of the user
            case 'changepw':

                //Get the password details to change it
                $id = $this->input->post('id');
                $pwd = $this->input->post('password');
                $conf_pwd = $this->input->post('conf_pword');

                //Send the password details to change
                $msg = $this->authlib->changePw($id, $pwd, $conf_pwd);

                if ($msg === true) {
                    $response["Status"] = "done"; //If the pasword was changed successfully the status is done
                } else {
                    $response["Status"] = $msg;
                }
                echo json_encode($response); //Send the results as a Jason encoded
                break;

            //Ask questions
            case 'ask' :
                //Get the details of asked question
                $title = $this->input->post('title');
                $description = $this->input->post('description');
                $selectedSubject = $this->input->post('selectedSubject');
                $tags = $this->input->post('tags');


                if ($title == '' || $description == '' || $selectedSubject == '0' || $tags == '') {
                    $msg = 'Please complete all the fields';
                } else {
                    $msg = $this->QuestionModel->askQuestion($title, $description, $selectedSubject, $tags);
                }

                if ($msg === true) {
                    $response["Status"] = "success";
                } else {
                    $response["Status"] = $msg;
                }
                echo json_encode($response); //Send the results as a Jason encoded

                break;
            // Vote for questions
            case 'question':

                $id = $this->input->post('id');
                $votes = $this->input->post('votes');
                $uid = $this->input->post('uid');
                $owner = $this->input->post('owner');
                $voteFor = QUESTION;

                $response["Status"] = "An Error Occurred.";

                $recordVote = $this->UserModel->recordVotedUser($uid, $id, $voteFor, $owner); //Record the votes user
                $response["Status"] = $recordVote;
                if ($recordVote === TRUE) {
                    $response["Status"] = $this->VoteModel->vote($args, $id, $votes); //Record the vote
                }
                if ($recordVote === TRUE) {
                    $response["Status"] = $this->UserModel->calcReputation($owner, $votes); //Calculate the reputation according to vote
                }

                echo json_encode($response); //Send the results as a Jason encoded
                break;

            //Vote for answers
            case 'answer':

                $id = $this->input->post('id');
                $votes = $this->input->post('votes');
                $uid = $this->input->post('uid');
                $owner = $this->input->post('owner');
                $voteFor = ANSWER;

                $response["Status"] = "An Error Occurred.";

                $recordVote = $this->UserModel->recordVotedUser($uid, $id, $voteFor, $owner); //Record the voted user
                $response["Status"] = $recordVote;
                if ($recordVote === TRUE) {
                    $response["Status"] = $this->VoteModel->vote($args, $id, $votes); //Record the vote
                }
                if ($recordVote === TRUE) {
                    $response["Status"] = $this->UserModel->calcReputation($owner, $votes); // Calculate reputation according to vote
                }

                echo json_encode($response); //Send the results as a Jason encoded

                break;

            //Answer for a question
            case 'answers':
                $answer = $this->input->post('answer');
                $uid = $this->input->post('uid');
                $qid = $this->input->post('ques');

                $ansId = $this->AnswerModel->answer($uid, $qid, $answer); //Add answer to answer table
                $an = $this->QuestionModel->addAnsId($qid, $ansId); // Add answer id to questions table
                if ($an)
                    $response["Status"] = "success";
                else
                    $response["Status"] = "not";
                echo json_encode($response); //Send the results as a Jason encoded
                break;

            // Change user details
            case 'change':
                $name = $this->input->post('name');
                $dob = $this->input->post('dateDob');
                $favouriteSubject = $this->input->post('selectedSubject');

                $msg = $this->authlib->change($name, $dob, $favouriteSubject); //Send user details to change

                if ($msg === true) {
                    $response["Status"] = "done";
                } else {
                    $response["Status"] = $msg;
                }

                echo json_encode($response); //Send the results as a Jason encoded
                break;

            //Delete user account
            case 'deleteuser' :

                $id = $this->input->post('id');

                $msg = $this->authlib->deleteUser($id); //Send user id to delete
                echo json_encode($msg); //Send the results as a Jason encoded
                break;
            //Close a question
            case 'closequestion':
                $id = $this->input->post('id');
                $msg = $this->QuestionModel->closeQuestion($id); //Send question id to close
                echo json_encode($msg); //Send the results as a Jason encoded
                break;
            //Accpet an answer
            case 'acceptanswer':
                $msg = '';
                $id = $this->input->post('id');
                $ansid = $this->input->post('ansid');
                $msg = $this->QuestionModel->acceptAnswer($id, $ansid); //Sens answer id to accept
                echo json_encode($msg);//Send the results as a Jason encoded
                break;

            //Delete a question
            case 'deletequestion':
                $id = $this->input->post('id');
                $uid = $this->input->post('uid');
                $msg = $this->QuestionModel->deleteQuestion($id, $uid); //Send question id and logged in user id to delete

                echo json_encode($msg); //Send the results as a Jason encoded
                break;

            //delete an answer
            case 'deleteanswer':
                $id = $this->input->post('id');
                $uid = $this->input->post('uid');
                $msg = $this->QuestionModel->deleteAnswer($id, $uid); //Send qestion and answer id to delete

                if ($msg === TRUE)
                    $msg = "Successfully deleted the answer.";

                echo json_encode($msg); //Send the results as a Jason encoded
                break;
            //Edit a question
            case 'editquestion':
                $id = $this->input->post('id');
                $title = $this->input->post('title');
                $description = $this->input->post('description');
                $msg = $this->QuestionModel->editQuestion($id, $title, $description); //Send question details to edit
                if ($msg === TRUE)
                    $msg = "Successfully edited the question.";

                echo json_encode($msg); //Send the results as a Jason encoded
                break;
            //Edit an answer    
            case 'editanswer':
                $id = $this->input->post('id');
                $answer = $this->input->post('answer');

                $msg = $this->QuestionModel->editAnswer($id, $answer); //Send answer details to edit
                if ($msg === TRUE)
                    $msg = "Successfully edited the answer.";

                echo json_encode($msg); //Send the results as a Jason encoded
                break;
            //Forgot password
            case 'forgotpw':
                $email = $this->input->post('forgotEmail');
                $msg = $this->authlib->validateEmail($email); //Send new password details to send a new password
                echo json_encode($msg); //Send the results as a Jason encoded
                break;
            default:
                show_error('Unsupported resource-post', 404);
                break;
        }
    }

}

?>
