<?php

class HomeController extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('QuestionModel');
        $this->load->model('AnswerModel');
    }

    //Load homepage
    public function index() {
        
        $questions = $this->QuestionModel->getNumofQuestions();
        $answers = $this->AnswerModel->getNumofAns();
        $data = array(
            'question' => $questions,
            'answer' => $answers
        );
        //Show the number of questions and answers currently asked and answered.
        $this->createViewWithArray("homeview", $data);
    }    
}

?>
