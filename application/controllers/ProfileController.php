<?php

class ProfileController extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->ci = &get_instance();
        $this->ci->load->model('QuestionModel');
         $this->ci->load->model('UserModel');
    }

    //View the profile of currently logged in user
    public function profileView($userId) {
        $this->createViewWithArraynStr("profileView", $this->showSubjects(), $userId);
    }

    //Get all the subjects
    function showSubjects() {
        $query = $this->QuestionModel->getSubjects();
        return $query;
    }

    //Get the profile of currently logged in user
    public function getuser() {
        $userId = $this->input->post('usr');
        if ($userId != "")
            $this->profileView($userId);
    }

    //View all the tutors
    public function viewtutors() {
        $data = array();
        $data = $this->checkGroup(TUTOR_GROUP);
        $this->createViewWithArray("usersview", $data);
    }

    //View all the students
    public function viewstudents() {

        $user = $this->authlib->is_loggedin();

        if ($user == false) { //If the user is not signed in students will not be viewed.
            $this->createViewWithString("errorview", "Please sign in to view students.");
        } else {
            $data = array();
            $data = $this->checkGroup(STUDENT_GROUP);
            $this->createViewWithArray("usersview", $data);
        }
    }

    //Get the user group of currently logged in user
    public function checkGroup($type) {

        $user = $this->authlib->is_loggedin();

        $data['grp'] = '';

        if ($user !== false) {
            $userGroup = $this->authlib->checkUserGroup($user);
            $result = (Array) $userGroup;
            $data['grp'] = $result['userGroup'];
        }
        $data['type'] = $type;

        return $data;
    }
    
    //Get the details of surrently logged in user
    public function myProfileView(){
        $userId = $this->UserModel->getloggedUserID();

        if ($userId == false) { // An error page will be displayed if the user is not logged in
            $this->createViewWithString("errorview", "Please sign in to view your profile.");
        } else {

            $this->createViewWithArray("profileView", $this->showSubjects());
        }        
    }
}

?>
