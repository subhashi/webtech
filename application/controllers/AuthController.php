<?php

class AuthController extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->ci = &get_instance();
        $this->ci->load->model('QuestionModel');
    }

    //Register user
    public function register() {
        $user = $this->authlib->is_loggedin();

        if ($user == false) { 
            $this->createViewWithArray("registerview", $this->showSubjects());
        } else {//If user is already signed in goes to the error page
            $userGroup = $this->createViewWithString("errorview", "You have already registered and signed in. Enjoy browsing the site.");
        }
    }

    //Login user
    public function login() {
        $this->authenticate();
    }

    //Authenticate user using email and password
    public function authenticate() {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $this->authlib->login($email, $password);
    }

    //Logout user
    public function logout() {
        $this->session->sess_destroy();
        $this->login();
    }

    //Get all the subjects
    function showSubjects() {
        $query = $this->QuestionModel->getSubjects();
        return $query;
    }
}

?>