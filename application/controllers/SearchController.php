<?php

class SearchController extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->ci = &get_instance();
        $this->ci->load->model('QuestionModel');
    }
    
    public function index(){        
         $this->createView("searchview");
    }
    
    //View the page of basic search with the search term
    public function basicSearch($searchTerm) {
        $this->createViewWithArraynStr("searchview",$this->showSubjects(),$searchTerm);
    }
    
    //Get the searchterm from the input
    public function getSearchTerm(){
        $searchTerm = $this->input->get('searchTerm');
        $this->basicSearch($searchTerm);
    }
    
    //Show all the subjects
    function showSubjects() {       
        $query = $this->QuestionModel->getSubjects();        
        return $query;
    }
    
    
}
?>
