<?php

class Authlib {

    function __construct() {
        $this->ci = &get_instance();
        $this->ci->load->model('UserModel');
    }

    //Regiter the user
    public function register($name, $email, $pwd, $conf_pwd, $dob, $favouriteSubject, $userGroup) {
        if ($name == '' || $email == '' || $pwd == '' || $conf_pwd == '' || $dob == '' || $favouriteSubject == '0' || $userGroup == '') {
            return 'Please fill all the fields';
        }
        if ($pwd != $conf_pwd) {
            return "Passwords do not match";
        }
        return $this->ci->UserModel->register($name, $email, $pwd, $dob, $favouriteSubject, $userGroup);
    }

    //Login
    public function login($email, $pwd) {
        if ($email == '' || $pwd == '') {
            return "Please enter email and password";
        }
        return $this->ci->UserModel->login($email, $pwd);
    }

    //Check whether the user is currently logged in
    public function is_loggedin() {
        return $this->ci->UserModel->is_loggedin();
    }

    //Chnage the user password
    public function changePw($id, $pwd, $conf_pwd) {

        if ($pwd == '' || $conf_pwd == '') {
            return 'Please fill all the fields';
        }

        if ($pwd != $conf_pwd) {
            return "Passwords do not match";
        }
        return $this->ci->UserModel->changePw($id, $pwd);
    }

    //Change the user details
    public function change($name, $dob, $favouriteSubject) {
        if ($name == '' || $dob == '' || $favouriteSubject == '0') {
            return 'You cannot keep any field blank';
        }
        return $this->ci->UserModel->change($name, $dob, $favouriteSubject);
    }

    //Delete user account
    public function deleteUser($id) {

        $getLoggedUser = $this->ci->UserModel->getloggedUserID();//Check whether user is logged in

        if ($getLoggedUser == false) {
            return 'Please sign in, to delete the account';
        } else {
            $loggedUserEmail = $this->is_loggedin();

            $userGroup = (Array) $this->checkUserGroup($loggedUserEmail);
            $userGroup = $userGroup['userGroup'];

            if ($id == $getLoggedUser) {
                return $this->ci->UserModel->deleteUser($id);
            } else if ($userGroup == ADMIN_GROUP) {
                return $this->ci->UserModel->deleteUser($id);
            }
        }

        return "You are not authorized to delete this account.";
    }

    //Check user's group
    public function checkUserGroup($user) {
        return $this->ci->UserModel->checkUserGroup($user);
    }

    //Validate the email address
    function validateEmail($email) {
        if ($email == '')
            return "Please enter email address.";
        $newPw = $this->ci->UserModel->validateEmail($email);

        if ($newPw == false)
            return "Sorry email does not exist";
        else
            return $this->resetpw($email, $newPw);
    }

    //Send a new password as an email, if password is forgotten
    function resetpw($email, $newPw) {

        // parameters of your mail server and how to send your email
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'rapburner@gmail.com',
            'smtp_pass' => 'Share123',
            'mailtype' => 'html'
        );

        // recipient, sender, subject, and you message
        
        $to = $email;
        $from = "rapburner@gmail.com";
        $subject = "Reset password for parapsyc.com";
        $message = "Use this password to log in: \r\n" . $newPw;

        // load the email library that provided by CI
        $this->ci->load->library('email', $config);
        // this will bind your attributes to email library
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from($from, 'parapsyc.com');
        $this->ci->email->to($to);
        $this->ci->email->subject($subject);
        $this->ci->email->message($message);

        // send your email. if it produce an error it will print 'Fail to send your message!' for you
        if ($this->ci->email->send()) {
            
            return "Password was sent successfully!";
        } else {
            echo "Fail to send your password!";
        }
    }

    //Get the user id of the logged in user
    function getloggedUserID(){
        return $this->ci->UserModel->getloggedUserID();
    }   
}

?>
