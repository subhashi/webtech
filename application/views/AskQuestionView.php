<script type="text/javascript">
//    post the question
    function postQuestion() {
        var formData = $("#askForm").serializeArray();

        var URL = "<?php echo base_url('/index.php/rest/ask'); ?>";
        $.post(URL,
                formData,
                function(data)
                {
                    var jsonData = jQuery.parseJSON(data);
                    if (jsonData.Status === "success") {
                        alert("successfully posted");
                        location.replace("/webtech/index.php/questioncontroller/questionview");
                    }
                    else {
                        document.getElementById('msg').innerHTML = jsonData.Status;
                    }

                }).fail(function(data)
        {
            var jsonData = jQuery.parseJSON(data);
            alert(data);
        });
    }

</script>

<div class="content">
    <div class="container">

        <div class="row">
            <div class="span6 offset3">
                <span id="pls-login"> </span>
                <h4 class="widget-header"> Ask Questions</h4>
                <div class="widget-body">

                    <form  id ="askForm" method="POST" class="form-horizontal form-signin-signup">
                        <table>
                            <tr>
                                <td>Title</td>
                                <td><input type="text" name='title' length="50" size="50" id='title'></td>
                            </tr>
                            <tr>
                                <td>Description</td>                                
                                <td><textarea name='description' rows="4" cols="300" it='des' value=''></textarea></td>
                            </tr>
                            <tr>
                                <td>Subject</td>
                                <td>
                                    <?php
                                    echo "<select name='selectedSubject'>";

                                    $co = 0;

                                    echo '<option value="' . $co . '">Select Subject</option>';
                                    foreach ($subject as $item) {
                                        $co = $co + 1;
                                        echo '<option value="' . $co . '" >' . $item . '</option>';
                                    }
                                    ?>

                                </td>
                            </tr>
                            <tr>
                                <td>Tags</td>                               
                                <td><input type="text" id='tags' name='tags' length="15" size="50">                             
                                    <i>Enter keywords in your question (Separate by spaces)</i>
                                </td>
                            </tr>
                            <tr>  
                                <td></td> 
                                <td>   
                                    <input type="button" id ="askButton"  value='Ask' class="btn btn-primary btn-large" onclick="postQuestion();">
                                    <?php if ($isLogged == "no") { ?>
                                        <script type="text/javascript">
                                            document.getElementById("pls-login").innerHTML = "Please Sign In to ask questions";
                                            document.getElementById("askButton").disabled = true;
                                        </script>
                                    <?php } else {
                                        ?>
                                        <script type="text/javascript">
                                            document.getElementById("askButton").disabled = false;                                            
                                        </script>
                                    <?php } ?>
                                </td>                                
                            </tr>
                            <tr>
                                <td></td>
                                <td><div style="color: red" id="msg"></div><br></td>                                
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div> 
</div>

