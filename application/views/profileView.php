<script type="text/javascript">

    var prevbtn = '<input type="button" class="btn pg-btn login-button" ';
    var searchType = 'onclick="';
    var prevbtn_close = '(this.id);" id="' + FIRST_PAGE + '" value="<<">';
    var pageId = '<input type="button" class="btn login-button pg-btn" id="';
    var btnOnclick = '" onclick="';
    var btnOnclick_close = '(this.id);"';
    var pageNo = ' value="';
    var pageNo_close = '">';

    var lastbtn = '<input type="button" class="btn login-button pg-btn"'
    var lastbtn_close = '(this.id);" id="' + numOfPage + '" value=">>">';

    var numOfPage;
    var MAX_PAGE_NUMBERS = 5;
    var FIRST_PAGE = 1;
    var BASIC_SEARCH_TYPE = 'getSearch';
    var ADVANCED_SEARCH_TYPE = 'advanceSearch';

    function getSearch(pageNum) {

        var URL = "<?php echo base_url('index.php/rest/searchspecific/label/'); ?>" + 'question' + "/term/" + 'askedBy' + "/pg/" + pageNum;

        $.get(URL,
                function(data)
                {
                    var jsonData = jQuery.parseJSON(data);
                    $('#pages').empty();
                    $('#questions').empty();
                    if (jsonData.results === false) {
                        var fragment = create("You have not asked any question yet");
                        document.getElementById("questions").appendChild(fragment);

                    } else {
                        var subjectNames = jsonData.subjects;
                        var userNames = jsonData.users;
                        var tags = jsonData.tags;
                        numOfPage = jsonData.numOfPage;


                        displayQuestions(jsonData.results, subjectNames, userNames, tags);
                        if (numOfPage > 1)
                            pagination(pageNum, 0);
                    }
                }).fail(function(data)
        {
            var jsonData = jQuery.parseJSON(data);
            if (jsonData.Status === "noResult") {

            }
        });
    }

    function pagination(pageNum, typsOfSearch) {

        var type;
        if (+typsOfSearch === 0)
            type = BASIC_SEARCH_TYPE;
        else
            type = ADVANCED_SEARCH_TYPE;

        var pages = '';
        if (numOfPage <= MAX_PAGE_NUMBERS) {

            for (var j = 1; j <= numOfPage; j++) {
                pages = pages + pageId + j + btnOnclick + type + btnOnclick_close + pageNo + j + " " + pageNo_close;
            }
            var fragment = create(pages);
            document.getElementById("pages").appendChild(fragment);
        }
        else {
            paginationForLot(pageNum);
        }
    }

    function paginationForLot(pageNum) {
        var type;

        if (pageNum < MAX_PAGE_NUMBERS - 2) {

            var start = 1;
            var end = MAX_PAGE_NUMBERS;

            for (var j = start; j <= end; j++) {
                pages = pages + pageId + j + btnOnclick + 'getSearch' + btnOnclick_close + pageNo + j + " " + pageNo_close;
            }
            pages = pages + lastbtn + searchType + type + lastbtn_close;

            var fragment = create(pages);
            document.getElementById("pages").appendChild(fragment);
        }
        else {
            var start = +pageNum - 2;
            var end = +pageNum + 2;

            if (end > numOfPage)
                end = numOfPage;

            for (var j = start; j <= end; j++) {
                pages = pages + pageId + j + btnOnclick + type + btnOnclick_close + pageNo + j + " " + pageNo_close;
            }
            pages = prevbtn + searchType + type + prevbtn_close + pages + lastbtn + searchType + type + lastbtn_close;

            var fragment = create(pages);
            document.getElementById("pages").appendChild(fragment);
        }
    }
    function displayQuestions(questionData, subjectNames, userNames, tags) {

        $('#questions').empty();
        var url = "<?php echo base_url('index.php/QuestionController/loadSelectedQues'); ?>";
        var questionid = ' <li style = "display: block;"><div><form action="' + url + '" id="questionIdForm';
        var formId = '" method="get" ><input type="hidden" name="questionId" value="';
        var questionidclose = '"></form>';

        var divNumofVotes = '<div class="votes-answers"> <div class="votes-box"> <div class="num-votes">';
        var divVotes = '</div><div class="votes-answers-font">votes</div></div>';
        var divNumofAns = '<div class="answers-box"><div class="votes-answers-font">';
        var divEndAns = '<br>answers</div></div></div>';

        var questionTitle = '<div class="question-box" ><div class="question-box2"><div class="question-font" id="';
        var questionIdClose = '" onclick="submitSelectedQuestion(this.id);" style="cursor: pointer;"><a>';
        var questionSubject = '</a></div><div><div class="question-subject">';
        var questionTag = '';
        var questionDescription = '</div></div></div><br>';

        var questionAskedBy = '<br><div class="question-asked-by">';
        var closeQuestions = '</div><hr></div></div></div></li>';


        for (var i = 0; i < questionData.length; i++) {

            if (tags[i].indexOf(",") !== -1) {
                var res = tags[i].split(',');
                questionTag = '</div>';
                for (var j = 0; j < res.length; j++) {
                    questionTag = questionTag + '<div class="question-tag">' + res[j] + '</div>';
                }
                questionTag = questionTag + '<div>';
            }
            else {
                questionTag = '</div><div class="question-tag">' + tags[i];
            }

            var NumOfVotesAndAns = questionid + questionData[i].id + formId + questionData[i].id + questionidclose + divNumofVotes + questionData[i].votes + divVotes + divNumofAns + countAns(questionData[i].answers) + divEndAns;

            var question = questionTitle + questionData[i].id + questionIdClose + questionData[i].title + questionSubject + subjectNames[+questionData[i].subject] + questionTag + questionDescription + questionData[i].description;

            var askedBy = questionAskedBy + userNames[i].name + " --- " + questionData[i].askedDate + closeQuestions;

            var questionSlot = NumOfVotesAndAns + question + askedBy;
            var fragment = create(questionSlot);

            document.getElementById("questions").appendChild(fragment);
        }
    }

    function create(htmlStr) {
        var frag = document.createDocumentFragment();
        temp = document.createElement('div');

        temp.innerHTML = htmlStr;
        while (temp.firstChild) {
            frag.appendChild(temp.firstChild);
        }
        return frag;
    }


    function countAns(answers) {
        if (answers !== "")
            return answers.split(",").length;
        else
            return '0';
    }
    function submitSelectedQuestion(id) {
        var id = "questionIdForm" + id;
        document.getElementById(id).submit();
    }

    function getUserData() {

        var userId = document.getElementById("userId").value;

        var URL = "<?php echo base_url('index.php/rest/user/id/'); ?>" + userId;

        $.get(URL,
                function(data)
                {
                    var jsonData = jQuery.parseJSON(data);

                    var users = jsonData.users;
                    setUserDetails(users);
                    getSearch(1);
                }).fail(function(data)
        {
            var jsonData = jQuery.parseJSON(data);
            if (jsonData.Status === "noResult") {

            }
        });
    }

    function changePassword() {
        var userId = document.getElementById("userId").value;
        var password = document.getElementById("password").value;
        var conf_pword = document.getElementById("conf_pword").value;

        var URL = "<?php echo base_url('index.php/rest/changepw'); ?>";
        $.post(URL,
                {'id': userId, 'password': password, 'conf_pword': conf_pword},
        function(data)
        {

            var jsonData = jQuery.parseJSON(data);
            if (jsonData.Status === "done") {
                document.getElementById('pwmsg').style.color = "green";
                document.getElementById('pwmsg').innerHTML = 'Password Successfully Changed';
                document.getElementById("password").value = '';
                document.getElementById("conf_pword").value = '';
            }
            else {
                document.getElementById('pwmsg').style.color = "red";
                document.getElementById('pwmsg').innerHTML = jsonData.Status;
                document.getElementById("password").value = '';
                document.getElementById("conf_pword").value = '';
            }

        }).fail(function()
        {
        });
    }

    function setUserDetails(users) {
        document.getElementById("name").value = users[0].name;
        document.getElementById("email").value = users[0].email;
        document.getElementById("dateDob").value = users[0].dob;
        document.getElementById("reputation").innerHTML = users[0].reputation;
        document.getElementById("joinedDate").innerHTML = users[0].joinedDate;
        document.getElementById("selectedSubject").value = users[0].favouriteSubject;
    }

    function changeUser() {
        var formData = $("#changeForm").serializeArray();

        var URL = "<?php echo base_url('index.php/rest/change'); ?>";
        $.post(URL,
                formData,
                function(data)
                {

                    var jsonData = jQuery.parseJSON(data);
                    if (jsonData.Status === "done") {
                        document.getElementById('msg').style.color = 'green';
                        document.getElementById('msg').innerHTML = "Successfully Changed Details!";
                    } else {
                        document.getElementById('msg').style.color = 'red';
                        document.getElementById('msg').innerHTML = jsonData.Status;
                    }
                }).fail(function(jqXHR, textStatus, errorThrown)
        {
        });
    }

    function getSubject(data) {
        var e = document.getElementById("subjects");
        var strUser = e.options[e.selectedIndex].value;

        document.getElementById('selectedOption').value = data;
    }

    function deleteAccount() {

        $(document).ready(function() {
            var boxWidth = 400;
            function centerBox() {
                /* Preliminary information */
                var winWidth = $(window).width();
                var winHeight = $(document).height();
                var scrollPos = $(window).scrollTop();
                /* auto scroll bug */
                /* Calculate positions */
                var disWidth = (winWidth - boxWidth) / 2;
                var disHeight = scrollPos + 150;
                /* Move stuff about */
                $('.popup-box').css({'width': boxWidth + 'px', 'left': disWidth + 'px', 'top': disHeight + 'px'});
                $('#blackout').css({'width': winWidth + 'px', 'height': winHeight + 'px'});
                return false;
            }
            $(window).resize(centerBox);
            $(window).scroll(centerBox);
            centerBox();
            $('[class*=popup-link]').click(function(e) {
                /* Prevent default actions */
                e.preventDefault();
                e.stopPropagation();
                /* Get the id (the number appended to the end of the classes) */
                var name = $(this).attr('class');
                var id = name[name.length - 1];
                var scrollPos = $(window).scrollTop();
                /* Show the correct popup box, show the blackout and disable scrolling */
                $('#popup-box-' + id).show();
                $('#blackout').show();
                $('html,body').css('overflow', 'hidden');
                /* Fixes a bug in Firefox */
                $('html').scrollTop(scrollPos);
            });
            $('[class*=popup-box]').click(function(e) {
                /* Stop the link working normally on click if it's linked to a popup */
                e.stopPropagation();
            });
            $('html').click(function() {
                var scrollPos = $(window).scrollTop();
                /* Hide the popup and blackout when clicking outside the popup */
                $('[id^=popup-box-]').hide();
                $('#blackout').hide();
                $("html,body").css("overflow", "auto");
                $('html').scrollTop(scrollPos);
            });
            $('.close').click(function() {
                var scrollPos = $(window).scrollTop();
                /* Similarly, hide the popup and blackout when the user clicks close */
                $('[id^=popup-box-]').hide();
                $('#blackout').hide();
                $("html,body").css("overflow", "auto");
                $('html').scrollTop(scrollPos);
            });
        });
    }

    function deleteUser() {

        var id = document.getElementById("userId").value;
        var URL = "<?php echo base_url('index.php/rest/deleteuser'); ?>";

        var postData = {
            "id": id
        };

        $.post(URL,
                postData,
                function(data)
                {
                    if (data)
                        logOut();
                    else
                        alert(data);


                }).fail(function(data)
        {
            alert("ERROR!\n" + data);
        });

    }

    function logOut() {

        $.ajax({
            url: "<?php echo base_url('index.php/AuthController/logout'); ?>"
        }).done(function(data) {
            window.location.href = "<?php echo base_url('index.php'); ?>";
        });
    }

    $(document).ready(function() {
        var boxWidth = 400;
        function centerBox() {
            /* Preliminary information */
            var winWidth = $(window).width();
            var winHeight = $(document).height();
            var scrollPos = $(window).scrollTop();
            /* auto scroll bug */
            /* Calculate positions */
            var disWidth = (winWidth - boxWidth) / 2;
            var disHeight = scrollPos + 150;
            /* Move stuff about */
            $('.popup-box').css({'width': boxWidth + 'px', 'left': disWidth + 'px', 'top': disHeight + 'px'});
            $('#blackout').css({'width': winWidth + 'px', 'height': winHeight + 'px'});
            return false;
        }
        $(window).resize(centerBox);
        $(window).scroll(centerBox);
        centerBox();
        $('[class*=popup-link]').click(function(e) {
            /* Prevent default actions */
            e.preventDefault();
            e.stopPropagation();
            /* Get the id (the number appended to the end of the classes) */
            var name = $(this).attr('class');
            var id = name[name.length - 1];
            var scrollPos = $(window).scrollTop();
            /* Show the correct popup box, show the blackout and disable scrolling */
            $('#popup-box-' + id).show();
            $('#blackout').show();
            $('html,body').css('overflow', 'hidden');
            /* Fixes a bug in Firefox */
            $('html').scrollTop(scrollPos);
        });
        $('[class*=popup-box]').click(function(e) {
            /* Stop the link working normally on click if it's linked to a popup */
            e.stopPropagation();
        });
        $('html').click(function() {
            var scrollPos = $(window).scrollTop();
            /* Hide the popup and blackout when clicking outside the popup */
            $('[id^=popup-box-]').hide();
            $('#blackout').hide();
            $("html,body").css("overflow", "auto");
            $('html').scrollTop(scrollPos);
        });
        $('.close').click(function() {
            var scrollPos = $(window).scrollTop();
            /* Similarly, hide the popup and blackout when the user clicks close */
            $('[id^=popup-box-]').hide();
            $('#blackout').hide();
            $("html,body").css("overflow", "auto");
            $('html').scrollTop(scrollPos);
        });
    });

</script>

<body onload="getUserData();">
    <div class="content">
        <div class="container">            
            <div class="row">
                <div class="span6 offset3">                    
                    <h4 class="widget-header"> My Profile</h4>
                    <div class="widget-body">
                        <form id="changeForm"  method="POST" class="form-horizontal form-signin-signup">
                            <table>
                                <tr>
                                    <td>Name</td>
                                    <td><input title="Enter your full name"  type="text" id="name" name='name'  size="30"></td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td><input type="text" disabled="disabled" id='email' name='email'  size="30"></td>
                                </tr>
                                <tr>
                                    <td>Date of Birth</td>
                                    <td><input type="date" id="dateDob" name="dateDob"></td>
                                </tr>
                                <tr>
                                    <td>Favorite Subject</td>
                                    <td>                                        
                                        <?php
                                        echo "<select id='selectedSubject' name='selectedSubject' >";

                                        $co = 0;

                                        echo '<option value="' . $co . '">Select Subject</option>';
                                        foreach ($subject as $item) {
                                            $co = $co + 1;
                                            echo '<option value="' . $co . '" >' . $item . '</option>';
                                        }
                                        echo'</select>'
                                        ?>                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td>Reputation</td>
                                    <td><span id="reputation"></span></td>
                                </tr>
                                <tr>
                                    <td> A member since</td>
                                    <td><span id="joinedDate"></span></td>
                                </tr>
                                <tr>
                                    <td>Password</td>
                                    <td><a class="popup-link-1">Change Password</a></td>
                                </tr>                                
                                <tr>
                                    <td></td>
                                    <td>
                                        <input type="button" value='Change' class="btn btn-primary btn-large" onclick="changeUser();">
                                        <input type="button" value="Delete Account" class="btn btn-primary btn-large popup-link-2" onclick="deleteAccount();">
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><span id="msg"></span></td>
                                </tr>                                
                            </table>
                        </form>
                        <h4>Questions asked by you</h4><br>
                        <ul id='questions'> </ul>
                        <div id="pages" style="text-align: center"></div>

                        <div class="popup-box" id="popup-box-1">
                            <div class="close">X</div>
                            <div class="top">
                                <h2>Change Password</h2>
                            </div>
                            <div class="bottom">
                                <form> 
                                    <table style="text-align:left">
                                        <tr>
                                            <td>Password</td>
                                            <td>
                                                <input style="width: 100%;" type="password" id='password' name='password' size="30">
                                            </td>
                                        <tr>
                                            <td>Confirm Password</td>
                                            <td>
                                                <input type="password" style="width: 100%;" id='conf_pword' name='conf_pword' size="30">
                                            </td>                                            
                                        </tr>
                                    </table>
                                    <div>
                                        <input type="button" value="Change Password" id="popup-btn" class="btn login-button" onclick="changePassword();">
                                        <br>
                                        <span id="pwmsg"></span>
                                    </div>
                                </form>                             
                            </div>

                        </div>
                        <div id="blackout"></div>

                        <div class="popup-box" id="popup-box-2">
                            <div class="close">X</div>
                            <div class="top">
                                <h2>Delete Account</h2>
                            </div>
                            <div class="bottom">
                                <form> 
                                    Are you sure your want to <b>delete the account</b> from the system? <br>
                                    <input id="popup-btn" type="button" value="Yes" class="btn login-button" onclick="deleteUser();">
                                    <input id="popup-btn" type="button" value="No" class="btn login-button">
                                    <br>
                                    <span id="delmsg"></span>
                                </form>                             
                            </div>

                        </div>
                        <div id="blackout"></div>
                    </div>
                </div>
            </div>
        </div> 
    </div>

