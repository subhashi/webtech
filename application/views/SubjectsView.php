<script type="text/javascript">
    var subjectDesc;
    var subjectId = 1;


    var prevbtn = '<input type="button" class="btn pg-btn login-button" ';
    var searchType = 'onclick="';
    var prevbtn_close = '(this.id);" id="' + FIRST_PAGE + '" value="<<">';
    var pageId = '<input type="button" class="btn login-button pg-btn" id="';
    var btnOnclick = '" onclick="';
    var btnOnclick_close = '(this.id);"';
    var pageNo = ' value="';
    var pageNo_close = '">';

    var lastbtn = '<input type="button" class="btn login-button pg-btn"'
    var lastbtn_close = '(this.id);" id="' + numOfPage + '" value=">>">';

    var numOfPage;
    var MAX_PAGE_NUMBERS = 5;
    var FIRST_PAGE = 1;

    function getSearch(pageNum) {

        var URL = "<?php echo base_url('index.php/rest/searchspecific/label/'); ?>" + 'subject' + "/term/" + subjectId + "/pg/" + pageNum;

        $.get(URL,
                function(data)
                {
                    var jsonData = jQuery.parseJSON(data);
                    $('#pages').empty();
                    $('#questions').empty();
                    if (jsonData.results === false) {
                        var fragment = create("There are no questions available to this this subject.");
                        document.getElementById("questions").appendChild(fragment);

                    } else {
                        var subjectNames = jsonData.subjects;
                        var userNames = jsonData.users;
                        var tags = jsonData.tags;
                        numOfPage = jsonData.numOfPage;


                        displayQuestions(jsonData.results, subjectNames, userNames, tags);
                        if (numOfPage > 1)
                            pagination(pageNum);
                    }

                }).fail(function(data)
        {
            var jsonData = jQuery.parseJSON(data);
            if (jsonData.Status === "noResult") {

            }
        });
    }

    function pagination(pageNum) {

        var pages = '';
        if (numOfPage <= MAX_PAGE_NUMBERS) {

            for (var j = 1; j <= numOfPage; j++) {
                pages = pages + pageId + j + btnOnclick + 'getSearch' + btnOnclick_close + pageNo + j + " " + pageNo_close;
            }
            var fragment = create(pages);
            document.getElementById("pages").appendChild(fragment);
        }
        else {
            paginationForLot(pageNum);
        }
    }

    function paginationForLot(pageNum) {

        if (pageNum < MAX_PAGE_NUMBERS - 2) {

            var start = 1;
            var end = MAX_PAGE_NUMBERS;

            for (var j = start; j <= end; j++) {
                pages = pages + pageId + j + btnOnclick + 'getSearch' + btnOnclick_close + pageNo + j + " " + pageNo_close;
            }
            pages = pages + lastbtn + searchType + 'getSearch' + lastbtn_close;

            var fragment = create(pages);
            document.getElementById("pages").appendChild(fragment);
        }
        else {
            var start = +pageNum - 2;
            var end = +pageNum + 2;

            if (end > numOfPage)
                end = numOfPage;

            for (var j = start; j <= end; j++) {
                pages = pages + pageId + j + btnOnclick + 'getSearch' + btnOnclick_close + pageNo + j + " " + pageNo_close;
            }
            pages = prevbtn + searchType + 'getSearch' + prevbtn_close + pages + lastbtn + searchType + 'getSearch' + lastbtn_close;

            var fragment = create(pages);
            document.getElementById("pages").appendChild(fragment);
        }


    }
    function displayQuestions(questionData, subjectNames, userNames, tags) {

        $('#questions').empty();
        var url = "<?php echo base_url('index.php/QuestionController/loadSelectedQues'); ?>";
        var questionid = ' <li style = "display: block;"><div><form action="' + url + '" id="questionIdForm';
        var formId = '" method="get" ><input type="hidden" name="questionId" value="';
        var questionidclose = '"></form>';

        var divNumofVotes = '<div class="votes-answers"> <div class="votes-box"> <div class="num-votes">';
        var divVotes = '</div><div class="votes-answers-font">votes</div></div>';
        var divNumofAns = '<div class="answers-box"><div class="votes-answers-font">';
        var divEndAns = '<br>answers</div></div></div>';

        var questionTitle = '<div class="question-box" ><div class="question-box2"><div class="question-font" id="';
        var questionIdClose = '" onclick="submitSelectedQuestion(this.id);" style="cursor: pointer;"><a>';
        var questionSubject = '</a></div><div><div class="question-subject">';
        var questionTag = '';
        var questionDescription = '</div></div></div><br>';

        var questionAskedBy = '<br><div class="question-asked-by">';
        var closeQuestions = '</div><hr></div></div></div></li>';


        for (var i = 0; i < questionData.length; i++) {

            if (tags[i].indexOf(",") !== -1) {
                var res = tags[i].split(',');
                questionTag = '</div>';
                for (var j = 0; j < res.length; j++) {
                    questionTag = questionTag + '<div class="question-tag">' + res[j] + '</div>';
                }
                questionTag = questionTag + '<div>';
            }
            else {
                questionTag = '</div><div class="question-tag">' + tags[i];
            }

            var NumOfVotesAndAns = questionid + questionData[i].id + formId + questionData[i].id + questionidclose + divNumofVotes + questionData[i].votes + divVotes + divNumofAns + countAns(questionData[i].answers) + divEndAns;

            var question = questionTitle + questionData[i].id + questionIdClose + questionData[i].title + questionSubject + subjectNames[+questionData[i].subject] + questionTag + questionDescription + questionData[i].description;

            var askedBy = questionAskedBy + userNames[i].name + " --- " + questionData[i].askedDate + closeQuestions;

            var questionSlot = NumOfVotesAndAns + question + askedBy;
            var fragment = create(questionSlot);
            document.getElementById("questions").appendChild(fragment);
        }
    }

    function create(htmlStr) {
        var frag = document.createDocumentFragment();
        temp = document.createElement('div');

        temp.innerHTML = htmlStr;
        while (temp.firstChild) {
            frag.appendChild(temp.firstChild);
        }
        return frag;
    }

    function countAns(answers) {
        if (answers !== "")
            return answers.split(",").length;
        else
            return '0';
    }
    function submitSelectedQuestion(id) {
        var id = "questionIdForm" + id;
        document.getElementById(id).submit();
    }

    function getSubjects() {

        var URL = "<?php echo base_url('index.php/rest/subjects'); ?>";
        $.get(URL,
                function(data)
                {
                    var jsonData = jQuery.parseJSON(data);
                    var subjectNames = jsonData.subjects;

                    subjectDesc = jsonData.subjectDesc;
                    displaySubjects(subjectNames);
                    displayDesc(1);
                    getSearch(1);

                }).fail(function(data)
        {

            alert("data");
        });
    }

    function displaySubjects(subjectNames) {

        var subId = '<div style="cursor: pointer; padding-bottom: 10px;" onclick="displayDesc(this.id)" id="';
        var subName = '">';
        var subName_close = '</div>';

        for (var i = 1; i < 6; i++) {
            var subjects = subId + (i) + subName + subjectNames[i] + subName_close;
            var fragment = create(subjects);

            document.getElementById("sub").appendChild(fragment);
        }
    }

    function displayDesc(id) {

        subjectId = id;
        getSearch(1);
        var subId = '<div id="';
        var subName = '">';
        var subName_close = '</div>';

        var subjects = subId + "desc" + (id + 1) + subName + subjectDesc[id] + subName_close;
        var fragment = create(subjects);

        document.getElementById("desc").innerHTML = subjects;
    }

    function create(htmlStr) {
        var frag = document.createDocumentFragment();
        temp = document.createElement('div');

        temp.innerHTML = htmlStr;
        while (temp.firstChild) {
            frag.appendChild(temp.firstChild);
        }
        return frag;
    }

</script>

<body onload="getSubjects();">
    <div class="content">
        <div class="container">            
            <div class="row">
                <div class="span6 offset3">
                    <h4 class="widget-header"> Subjects</h4>
                    <div class="widget-body">
                        <div style="margin-bottom: 10px; height: 200px;">
                            <div id="sub"></div>
                            <div id="desc"></div>
                        </div>
                        <h4>Questions related to subject</h4><br>
                        <ul id='questions'> </ul>
                        <div id="pages" style="text-align: center"></div>                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
