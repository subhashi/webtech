
<script type="text/javascript">

    //Post the details of the registered user
    function posttRegisterUser() {
        var formData = $("#registerForm").serializeArray();
        var URL = "<?php echo base_url('index.php/rest/register'); ?>";
        $.post(URL,
                formData,
                function(data)
                {
                    var jsonData = jQuery.parseJSON(data);
                    if (jsonData.Status === "registered") {
                        alert("Successfully Registered");
                        location.replace("<?php echo base_url('index.php/homeController'); ?>");
                    }
                    else {
                        document.getElementById('msg').innerHTML = jsonData.Status;
                    }

                }).fail(function(jqXHR, textStatus, errorThrown)
        {
        });
    }

    //validate the email address
    function validateEmail(email) {
        if (email.match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$"))
        {
            var icon = '<i id="trophy7" class="fa fa-check fa-1x"></i>'
            document.getElementById("emailVal").innerHTML = icon + " A valid email";
            document.getElementById("emailVal").style.color = "green";
        }
        else
        {
            var icon = '<i id="trophy7" class="fa fa-times fa-1x"></i>'
            document.getElementById("emailVal").innerHTML = icon + " An invalid email";
            document.getElementById("emailVal").style.color = "red";
        }
    }

    //Check whether the password ans confirm password matches
    function matchPw(confirmPw) {
        if (confirmPw === document.getElementById("pw").value)
        {
            var icon = '<i id="trophy7" class="fa fa-check fa-1x"></i>'
            document.getElementById("pwVal").innerHTML = icon + " Match with password";
            document.getElementById("pwVal").style.color = "green";
        }
        else
        {
            var icon = '<i id="trophy7" class="fa fa-times fa-1x"></i>'
            document.getElementById("pwVal").innerHTML = icon + " Doesn't match with the password";
            document.getElementById("pwVal").style.color = "red";
        }

    }

    //Get the subjects
    function getSubject(data) {
        var e = document.getElementById("subjects");
        var strUser = e.options[e.selectedIndex].value;
        document.getElementById('selectedOption').value = data;
    }

</script>

<body>
    <div class="content">
        <div class="container">            
            <div class="row">
                <div class="span6 offset3">
                    <h4 class="widget-header"> Register</h4>
                    <div class="widget-body">
                        <form id="registerForm"  method="POST" class="form-horizontal form-signin-signup">
                            <table>
                                <tr>
                                    <td>Name</td>
                                    <td><input type="text" name='name' length="20" size="50"></td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td><input type="text" name='email' length="15" size="30" onkeypress="validateEmail(this.value);"><span id="emailVal" class='errorShow'></span></td>
                                </tr>
                                <tr>
                                    <td>Password</td>
                                    <td><input  id='pw' type="password" name='password' length="15" size="30"></td>
                                </tr>
                                <tr>
                                    <td>Confirm password</td>
                                    <td><input type="password" name='conf_pword' length="15" size="30" onchange="matchPw(this.value);"> <span id="pwVal" class='errorShow'></span></td>
                                </tr>
                                <tr>
                                    <td>Date of Birth</td>
                                    <td><input type="date" name="dateDob" placeholder="yyyy-mm-dd"></td>
                                </tr>
                                <tr>
                                    <td>Favorite Subject</td>
                                    <td>

                                        <?php
                                        echo "<select name='selectedSubject' >";

                                        $co = 0;

                                        echo '<option value="' . $co . '">Select Subject</option>';
                                        foreach ($subject as $item) {
                                            $co = $co + 1;
                                            echo '<option value="' . $co . '" >' . $item . '</option>';
                                        }
                                        ?>

                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>I want to register,</td>
                                    <td><input type="radio" name='userGroup' value="1">As a Student</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><input type="radio" name='userGroup' value="2">As a Tutor</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <input type="button" value='Register' class="btn btn-primary btn-large" onclick="posttRegisterUser();" style="float: left;">                                    
                                        <div style="color: red; margin-left: 100px; margin-top: 8px;" id="msg"></div> <br>
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div> 
    </div>
