<body>
    <div class="content">
        <div class="container">            
            <div class="row">
                <div class="span6 offset3">                    
                    <h4 class="widget-header"> <i id="trophy7" class="fa  fa-lock fa-1x"></i> Access Denied</h4>
                    <div class="widget-body">
                        <div id="err">
                        <?php echo $message; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>