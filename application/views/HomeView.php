<script src="<?php echo base_url('assets/js/responsiveslides.min.js') ?>"></script>
<script type="text/javascript">
    //View the slides on home page
    $(function() {
        $(".rslides").responsiveSlides();
    });
    
</script>
<body>
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="span6 offset3">                   
                    <div class="widget-body">
                        <div style="text-align: center;">
                        <ul class="rslides">
                            <li><img src="<?php echo base_url('assets/img/3.jpg') ?>" alt=""></li>
                            <li><img src="<?php echo base_url('assets/img/2.jpg') ?>" alt=""></li>
                            <li><img src="<?php echo base_url('assets/img/1.jpg') ?>" alt=""></li>
                        </ul>
                        </div>
                        <div style="text-align: center; height: 70px; background-color: #777; padding: 2px; margin-top: 20px; margin-bottom: 20px">
                           
                            <h2><?php echo $subject['answer']?> <font style="color: #ffffff">Answers</font> for <?php echo $subject['question']?> <a style="color: #ffffff;" href="<?php echo base_url('index.php/questioncontroller/questionview'); ?>">Questions</a>.</h2> 
                                
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>