<script type="text/javascript">

    var numOfPage;
    var MAX_PAGE_NUMBERS = 5;
    var TUTOR_GROUP_TYPE = 2;
    var FIRST_PAGE = 1;
    var GOLD_USER = 10;
    var SILVER_USER = 5;
    var BRONZE_USER = 3;


    function getUsers(pageNum) {
        var URL = "<?php echo base_url('index.php/rest/getusers/pg/'); ?>" + pageNum;
        var userType = document.getElementById("type").value;
        var type = {'type': userType};

        $.get(URL,
                type,
                function(data)
                {
                    var jsonData = jQuery.parseJSON(data);
                    var users = jsonData.users;
                    var subjectNames = jsonData.subjects;
                    numOfPage = jsonData.numOfPage;

                    displayUsers(users, subjectNames);
                    viewDeleteBtns()
                    if (numOfPage > 1)
                        pagination(pageNum);
                }).fail(function(data)
        {
            var jsonData = jQuery.parseJSON(data);
            if (jsonData.Status === "noResult") {

            }
        });
    }

    function pagination(pageNum) {

        var prevbtn = '<input type="button" class="btn login-button pg-btn" id="firstPage" value="<<" style="float: left">';
        var pageId = '<input type="button" onclick="getUsers(this.id);" class="btn login-button pg-btn" id="';
        var pageNo = '" value="';
        var pageNo_close = '">';
        var pages = '';
        var lastbtn = '<input type="button" id=">>" value="Next">';

        if (numOfPage <= MAX_PAGE_NUMBERS) {
            $('#pages').empty();
            for (var j = 1; j <= numOfPage; j++) {
                pages = pages + pageId + j + pageNo + j + " " + pageNo_close;
            }
            var fragment = create(pages);
            document.getElementById("pages").appendChild(fragment);
        }
        else {
            paginationForLot(pageNum);
        }
    }

    function paginationForLot(pageNum) {

        var prevbtn = '<input type="button" class="btn pg-btn login-button" onclick="getUsers(this.id);" id="' + 1 + '" value="<<">';
        var pageId = '<input type="button" onclick="getUsers(this.id);" class="btn login-button pg-btn" id="';
        var pageNo = '" value="';
        var pageNo_close = '">';
        var pages = '';
        var lastbtn = '<input type="button" class="btn login-button pg-btn"  onclick="getUsers(this.id);" id="' + numOfPage + '" value=">>">';

        if (pageNum < MAX_PAGE_NUMBERS - 2) {

            $('#pages').empty();

            var start = 1;
            var end = MAX_PAGE_NUMBERS;

            for (var j = start; j <= end; j++) {
                pages = pages + pageId + j + pageNo + j + " " + pageNo_close;
            }
            pages = prevbtn + pages + lastbtn;

            var fragment = create(pages);
            document.getElementById("pages").appendChild(fragment);
        }
        else {
            $('#pages').empty();
            var start = +pageNum - 2;
            var end = +pageNum + 2;

            if (end > numOfPage)
                end = numOfPage;

            for (var j = start; j <= end; j++) {
                pages = pages + pageId + j + pageNo + j + " " + pageNo_close;
            }
            pages = prevbtn + pages + lastbtn;

            var fragment = create(pages);
            document.getElementById("pages").appendChild(fragment);
        }
    }

    function displayUsers(users, subjectNames) {

        $('#details').empty();
        var name = '<div class="grid-6 grid grey"><h4 class="name-header">';
        var details = '</h4><p class="quote">';
        var interested = 'Interested In <br>';
        var joinedDate = '<br>A member since ';
        var trophy = '</p><div ><i id="trophy';
        var trophy_close = '" class="fa fa-trophy fa-3x"></i>';
        var reputation = '<h5 class="name-header">';
        var reputation_close = '</h5></div>';
        var deleteUser = '<input type="button" value="Delete" class="btn login-button deleteubtn popup-link-1" onclick="deleteUser(this.id);" id="';
        var deleteUser_close = '">';
        var details_close = '</div>';
        var detailSlot = '';

        for (var i = 0; i < users.length; i++) {
            detailSlot = name + users[i].name + details + interested + subjectNames[+users[i].favouriteSubject] + joinedDate + users[i].joinedDate + trophy + users[i].id + trophy_close + reputation + users[i].reputation + reputation_close + deleteUser + users[i].id + deleteUser_close + details_close;
            var fragment = create(detailSlot);
            document.getElementById("details").appendChild(fragment);
            changeTrophyColour(+users[i].reputation, users[i].id);
        }
    }

    function searchUser(pageNum) {

        var label = 'user' + document.getElementById("type").value;
        var term = document.getElementById('searchname').value;

        if (term === '') {
            alert('Please enter a name to search')
        } else {

            var URL = "<?php echo base_url('index.php/rest/searchspecific/label/'); ?>" + label + "/term/" + term + "/pg/" + pageNum;

            $.get(URL,
                    function(data)
                    {
                        var jsonData = jQuery.parseJSON(data);
                        var users = jsonData.results;
                        var subjectNames = jsonData.subjects;
                        numOfPage = jsonData.numOfPage;
                        if (users === false) {
                            $('#details').empty();
                            var fragment = create("Your search did not match any user name. Please try again, <ul><li>Checking the spellings of user name</li><li>Checking user type (student/ tutor)</li></ul> <a href=# onclick='reloadPg();'> View All </a> ");
                            document.getElementById("details").appendChild(fragment);

                        } else {
                            displayUsers(users, subjectNames);
                            viewDeleteBtns()
                            if (numOfPage > 1)
                                pagination(pageNum);
                        }

                    }).fail(function(data)
            {
                var jsonData = jQuery.parseJSON(data);
                if (jsonData.Status === "noResult") {

                }
            });
        }        
    }
    function reloadPg(){
        location.reload();
        }
    function deleteUser(id) {
        var confirmDel = confirm("Are you sure you want to delete the user?");

        if (confirmDel) {
            var URL = "<?php echo base_url('index.php/rest/deleteuser'); ?>";

            var postData = {
                "id": id
            };

            $.post(URL,
                    postData,
                    function(data)
                    {
                        if (!data)
                            alert(data);
                        else
                            location.reload();

                    }).fail(function(data)
            {
                alert("ERROR!\n" + data);
            });
        }
    }

    function create(htmlStr) {
        var frag = document.createDocumentFragment();
        temp = document.createElement('div');

        temp.innerHTML = htmlStr;
        while (temp.firstChild) {
            frag.appendChild(temp.firstChild);
        }
        return frag;
    }

    function changeTrophyColour(reputation, id) {
        var trophyId = 'trophy' + id;

        if (reputation >= GOLD_USER)
            document.getElementById(trophyId).style.color = 'gold';
        else if (reputation >= SILVER_USER)
            document.getElementById(trophyId).style.color = 'silver';
        else if (reputation >= BRONZE_USER)
            document.getElementById(trophyId).style.color = '#965A38';
        else if (reputation <= 0)
            document.getElementById(trophyId).style.color = '#F8F8FF';
        else
            document.getElementById(trophyId).style.color = '#F0E68C';
    }
    function viewDeleteBtns() {
        var grp = document.getElementById("grp").value;

        if (grp === '3') {

            var delbtn = document.getElementsByClassName('btn login-button deleteubtn popup-link-1');

            for (var i = 0; i < delbtn.length; i++) {
                delbtn[i].style.display = "block";
            }
        }
    }
</script>
<body onload="getUsers(FIRST_PAGE);">
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="span6 offset3">
                    <h4 class="widget-header" id="heading"><?php
                        if ($subject['type'] == '1')
                            echo 'Students';
                        else
                            echo 'Totors';
                        ?></h4>
                    <div class="widget-body">
                        <input type="hidden" id='type' value= <?php echo '"' . $subject['type'] . '"'; ?> >
                        <input type="hidden" id="grp" value=<?php echo '"' . $subject['grp'] . '"'; ?> >
                        <div class="search" style="margin-bottom: 20px;margin-left: 35px;">
                            <form  name="userSearch" class="navbar-form ">

                                <input type="text"  id="searchname" name="searchname" placeholder="Type name of the user you want to search" class="form-control" style="width: 37%;">

                                <input onclick="searchUser(1);" type="button" value="Find User" class="btn" style="margin-left: 17px;">
                            </form>
                        </div>
                        <div id="details" style="margin-left: 50px; float: left;"></div>

                        <div id="pages" style="text-align: center"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
