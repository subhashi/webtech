<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.pages.js') ?>"></script>

<script type="text/javascript">

    var numOfPage;
    var MAX_PAGE_NUMBERS = 5;
    var FIRST_PAGE = 1;

    //Get all the questions
    function getQuestions(pageNum) {

        var URL = "<?php echo base_url('index.php/rest/getquestion/pg/'); ?>" + pageNum;
        $.get(URL,
                function(data)
                {
                    var jsonData = jQuery.parseJSON(data);

                    var subjectNames = jsonData.subjects;
                    var userNames = jsonData.users;
                    var tags = jsonData.tags;
                    numOfPage = jsonData.numOfPage;

                    displayQuestions(jsonData.questions, subjectNames, userNames, tags);
                    if (numOfPage > 1)
                        pagination(pageNum);

                }).fail(function(data)
        {
            var jsonData = jQuery.parseJSON(data);
            if (jsonData.Status === "noResult") {

            }
        });
    }
    
    //Create pagination for pages less than 5
    function pagination(pageNum) {

        var prevbtn = '<input type="button" class="btn login-button pg-btn" id="firstPage" value="<<" style="float: left">'
        var pageId = '<input type="button" onclick="getQuestions(this.id);" class="btn login-button pg-btn" id="';
        var pageNo = '" value="';
        var pageNo_close = '">';
        var pages = '';
        var lastbtn = '<input type="button" id=">>" value="Next">';

        if (numOfPage <= MAX_PAGE_NUMBERS) {
            $('#pages').empty();
            for (var j = 1; j <= numOfPage; j++) {
                pages = pages + pageId + j + pageNo + j + " " + pageNo_close;
            }
            var fragment = create(pages);
            document.getElementById("pages").appendChild(fragment);
        }
        else {
            paginationForLot(pageNum);
        }
    }

    //Create pagination for pages more than 5
    function paginationForLot(pageNum) {

        var prevbtn = '<input type="button" class="btn pg-btn login-button" onclick="getQuestions(this.id);" id="' + 1 + '" value="<<">'
        var pageId = '<input type="button" onclick="getQuestions(this.id);" class="btn login-button pg-btn" id="';
        var pageNo = '" value="';
        var pageNo_close = '">';
        var pages = '';
        var lastbtn = '<input type="button" class="btn login-button pg-btn"  onclick="getQuestions(this.id);" id="' + numOfPage + '" value=">>">';

        if (pageNum < MAX_PAGE_NUMBERS - 2) {

            $('#pages').empty();

            var start = 1;
            var end = MAX_PAGE_NUMBERS;

            for (var j = start; j <= end; j++) {
                pages = pages + pageId + j + pageNo + j + " " + pageNo_close;
            }
            pages = prevbtn + pages + lastbtn;

            var fragment = create(pages);
            document.getElementById("pages").appendChild(fragment);
        }
        else {
            $('#pages').empty();
            var start = +pageNum - 2;
            var end = +pageNum + 2;

            if (end > numOfPage)
                end = numOfPage;

            for (var j = start; j <= end; j++) {
                pages = pages + pageId + j + pageNo + j + " " + pageNo_close;
            }
            pages = prevbtn + pages + lastbtn;

            var fragment = create(pages);
            document.getElementById("pages").appendChild(fragment);
        }
    }

    //Dynamically display questions
    function displayQuestions(questionData, subjectNames, userNames, tags) {
        $('#questions').empty();
        var url = "<?php echo base_url('index.php/QuestionController/loadSelectedQues'); ?>";
        
        var questionid = ' <li style = "display: block;"><div><form action="' + url + '" id="questionIdForm';
        var formId = '" method="get" ><input type="hidden" name="questionId" value="';
        var questionidclose = '"></form>';

        var divNumofVotes = '<div class="votes-answers"> <div class="votes-box"> <div class="num-votes">';
        var divVotes = '</div><div class="votes-answers-font">votes</div></div>';
        var divNumofAns = '<div class="answers-box"><div class="votes-answers-font">';
        var divEndAns = '<br>answers</div></div></div>';

        var questionTitle = '<div class="question-box" ><div class="question-box2"><div class="question-font" id="';
        var questionIdClose = '" onclick="submitSelectedQuestion(this.id);" style="cursor: pointer;"><a>';
        var questionSubject = '</a></div><div><div class="question-subject">';
        var questionTag = '';
        var questionDescription = '</div></div></div><br>';

        var questionAskedBy = '<br><div class="question-asked-by">';
        var closeQuestions = '</div><hr></div></div></div></li>';


        for (var i = 0; i < questionData.length; i++) {

            if (tags[i].indexOf(",") !== -1) {
                var res = tags[i].split(',');
                questionTag = '</div>';
                for (var j = 0; j < res.length; j++) {
                    questionTag = questionTag + '<div class="question-tag">' + res[j] + '</div>';
                }
                questionTag = questionTag + '<div>';
            }
            else {
                questionTag = '</div><div class="question-tag">' + tags[i];
            }
            var NumOfVotesAndAns = questionid + questionData[i].id + formId + questionData[i].id + questionidclose + divNumofVotes + questionData[i].votes + divVotes + divNumofAns + countAns(questionData[i].answers) + divEndAns;

            var question = questionTitle + questionData[i].id + questionIdClose + questionData[i].title + questionSubject + subjectNames[+questionData[i].subject] + questionTag + questionDescription + questionData[i].description;

            var askedBy = questionAskedBy + userNames[i].name + " --- " + questionData[i].askedDate + closeQuestions;

            var questionSlot = NumOfVotesAndAns + question + askedBy;
            var fragment = create(questionSlot);

            document.getElementById("questions").appendChild(fragment);
        }
    }

    //Get the selected question
    function submitSelectedQuestion(id) {
        var id = "questionIdForm" + id;
        document.getElementById(id).submit();
    }

    //Count the total number of answers
    function countAns(answers) {
        if (answers !== "")
            return answers.split(",").length;
        else
            return '0';
    }


    //Create the question slot as a div
    function create(htmlStr) {
        var frag = document.createDocumentFragment();
        temp = document.createElement('div');

        temp.innerHTML = htmlStr;
        while (temp.firstChild) {
            frag.appendChild(temp.firstChild);
        }
        return frag;
    }

</script>
<body onload="getQuestions(FIRST_PAGE);">

    <div class="content">
        <div class="container">            
            <div class="row">
                <div class="span6 offset3">              
                    <div class="widget-body">                        
                        <ul id='questions'> </ul>                        
                        <div id="pages" style="text-align: center;"></div>
                    </div>
                </div>
            </div>
        </div> 
    </div>
</body>