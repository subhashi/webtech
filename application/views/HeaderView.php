<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Parapsychology | A tutoring site on Parapsychology">
        <meta name="author" content="Subhashi">
        <title>Parapsyc.com</title>

        <link href="<?php echo base_url('assets/css/parapstyle.css') ?>" rel="stylesheet">
        <script src="<?php echo base_url('assets/js/jquery-1.9.1.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery-ui.js') ?>"></script>
        <link href="<?php echo base_url('assets/font-awesome/css/font-awesome.css') ?>" type="text/css" rel="stylesheet">
        <link href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css') ?>" type="text/css" rel="stylesheet">

        <script type="text/javascript">
            function posttUser() {
                var formData = $("#loginForm").serializeArray();

                var URL = "<?php echo base_url('index.php/rest/authenticate'); ?>";
                $.post(URL,
                        formData,
                        function(data)
                        {
                            var jsonData = jQuery.parseJSON(data);
                            if (jsonData.Status === "logged") {
                                location.reload();
                            }
                            else {
                                document.getElementById('login-msg').innerHTML = '<i id="trophy7" class="fa fa-times fa-1x"></i>' + ' ' + jsonData.Status;
                            }
                        }).fail(function(jqXHR, textStatus, errorThrown)
                {
                });
            }
            //logout function
            function logOut() {

                $.ajax({
                    url: "<?php echo base_url('index.php/AuthController/logout'); ?>"
                }).done(function(data) {
                    location.reload();
                });
            }
            
            //Reset password when it's forgotten
            function resetPassword() {
                var formData = $("#reserpw").serializeArray();
                document.getElementById('rpwmsg').innerHTML = 'Please wait...';

                var URL = "<?php echo base_url('index.php/rest/forgotpw'); ?>";
                $.post(URL,
                        formData,
                        function(data)
                        {
                            
                            document.getElementById('rpwmsg').innerHTML = data;
                            document.getElementById('forgotEmail').value = '';
                        }).fail(function(jqXHR, textStatus, errorThrown)
                {
                });
            }

            //change the class of navigation buttons on mousehover
            function changeClass(titleId) {
                document.getElementById("title" + titleId).className = "active";
            }

            //Remove the class of navigation buttons on mover out
            function removeClass(titleId) {
                document.getElementById("title" + titleId).className = "noClass";
            }

            $(document).ready(function() {
                var boxWidth = 400;
                function centerBox() {
                    /* Preliminary information */
                    var winWidth = $(window).width();
                    var winHeight = $(document).height();
                    var scrollPos = $(window).scrollTop();
                    /* auto scroll bug */
                    /* Calculate positions */
                    var disWidth = (winWidth - boxWidth) / 2;
                    var disHeight = scrollPos + 150;
                    /* Move stuff about */
                    $('.popup-box').css({'width': boxWidth + 'px', 'left': disWidth + 'px', 'top': disHeight + 'px'});
                    $('#blackout').css({'width': winWidth + 'px', 'height': winHeight + 'px'});
                    return false;
                }
                $(window).resize(centerBox);
                $(window).scroll(centerBox);
                centerBox();
                $('[class*=popup-link]').click(function(e) {
                    /* Prevent default actions */
                    e.preventDefault();
                    e.stopPropagation();
                    /* Get the id (the number appended to the end of the classes) */
                    var name = $(this).attr('class');
                    var id = name[name.length - 1];
                    var scrollPos = $(window).scrollTop();
                    /* Show the correct popup box, show the blackout and disable scrolling */
                    $('#popup-box-' + id).show();
                    $('#blackout').show();
                    $('html,body').css('overflow', 'hidden');
                    /* Fixes a bug in Firefox */
                    $('html').scrollTop(scrollPos);
                });
                $('[class*=popup-box]').click(function(e) {
                    /* Stop the link working normally on click if it's linked to a popup */
                    e.stopPropagation();
                });
                $('html').click(function() {
                    var scrollPos = $(window).scrollTop();
                    /* Hide the popup and blackout when clicking outside the popup */
                    $('[id^=popup-box-]').hide();
                    $('#blackout').hide();
                    $("html,body").css("overflow", "auto");
                    $('html').scrollTop(scrollPos);
                });
                $('.close').click(function() {
                    var scrollPos = $(window).scrollTop();
                    /* Similarly, hide the popup and blackout when the user clicks close */
                    $('[id^=popup-box-]').hide();
                    $('#blackout').hide();
                    $("html,body").css("overflow", "auto");
                    $('html').scrollTop(scrollPos);
                });
            });


        </script>
    </head>
    <body>
        <nav role="navigation" class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar nav-heading">
                    <div class="navbar-header">
                        <button type="button" data-toggle="collapse" data-target=".navbar-ex1-collapse" class="navbar-toggle">
                                                    <!--<img src="/webtech/images/parapsyc_com_logo.png" alt="parapsyc.com logo" width="50%" height="50%">-->
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span><span class="icon-bar">
                            </span><span class="icon-bar"></span>
                        </button>
                        <a href="<?php echo base_url('index.php') ?>" class="navbar-brand kickstrap-logo">Parapsyc.com</a>
                    </div>
                    <ul class="nav navbar-nav"> 
                        <li id="title1" onmouseover="changeClass(1);" onmouseout="removeClass(1)"><a href="<?php echo base_url('index.php/questioncontroller/askquestionview') ?>"><i class="fa fa-comment-o fa-1x"></i> Ask Questions</a></li>
                        <li id="title2" onmouseover="changeClass(2);" onmouseout="removeClass(2)"><a href="<?php echo base_url('index.php/questioncontroller/questionview') ?>">Questions</a></li>
                        <li id="title3" onmouseover="changeClass(3);" onmouseout="removeClass(3)"><a href="<?php echo base_url('index.php/subjectscontroller/subjectsview') ?>">Subjects</a></li>
                        <li id="title4" onmouseover="changeClass(4);" onmouseout="removeClass(4)"><a href="<?php echo base_url('index.php/ProfileController/viewtutors') ?>">Tutors</a></li>
                        <li id="title5" onmouseover="changeClass(5);" onmouseout="removeClass(5)"><a href="<?php echo base_url('index.php/ProfileController/viewstudents') ?>">Students</a></li>
                        <li id="title6" onmouseover="changeClass(6);" onmouseout="removeClass(6)"><a href="<?php echo base_url('index.php/authcontroller/register') ?>">Register</a></li>
                        <li id="title7" onmouseover="changeClass(7);" onmouseout="removeClass(7)"><a href="<?php echo base_url('index.php/ProfileController/myProfileView') ?>">My Profile</a></li>
                        <!--href="/webtech/index.php/profilecontroller/profileview"-->
                    </ul>

                    <?php if ($isLogged == "no") { ?>
                        <script type="text/javascript">
                document.getElementById("title6").style.display = "inherit";
                document.getElementById("title7").style.display = "none";
                        </script>

                        <form method="POST" id="loginForm" class="navbar-form navbar-right ng-pristine ng-valid login-form">
                            <input type="text" name="email" placeholder="Email" class="form-control login-form-controll">
                            <input type="password" name="password" placeholder="Password" class="form-control login-form-controll">                                    
                            <input type="button" value="Sign In" class="btn login-button" onclick="posttUser();" style="width: 80px;">


                            <a href="#" class="popup-link-5"><input type='image' class="btn pwforgot" src="<?php echo base_url('assets/img/fp.png') ?>"></a>
                            <br>
                            <span class="login-error-msg" style="color: red" id="login-msg"></span>
                            <input type="hidden" id="userId" value = "" >

                        </form>

                    <?php } else {
                        ?>
                        <script type="text/javascript">
                document.getElementById("title6").style.display = "none";
                document.getElementById("title7").style.display = "inherit";
                        </script>
                        <form id="loginOutForm" method="GET" class="navbar-form navbar-right ng-pristine ng-valid"> 

                            <div class="badge">
                                <?php echo "Hello " . $isLogged . "!"; ?>
                            </div>
                            <input type="button" value="Sign Out" method="GET" class="btn login-button" onclick="logOut();">

                            <input type="hidden" id="userId" value = <?php echo '"' . $id . '"'; ?> >
                        </form>



                    <?php } ?>
                </div>
            </div>
            <div align="center" class="navbar-search">
                <form action="<?php echo base_url('index.php/SearchController/getSearchTerm'); ?>" method="GET" name="searchFormBasic" class="navbar-form " id="searchFormBasic">

                    <input type="text"  name="searchTerm" placeholder="Type a keyword/ phrase you want to search" class="form-control basic-search">

                    <input type="submit" value="Search" class="btn btn-search"><input type='image' class="btn adseaech-icon" src="<?php echo base_url('assets/img/adsearch.png') ?>">
                </form>
            </div>
        </nav>


        <div class="popup-box" id="popup-box-5">
            <div class="close">X</div>
            <div class="top">
                <h2>Reset Password</h2>
            </div>
            <div class="bottom">
                <form id ='reserpw'> 
                    <table style="text-align:left">
                        <tr>
                            <td colspan='2'>A new password will be sent to your email address.</td>
                        </tr>
                        <tr>
                            <td>Enter registered email address</td>
                            <td>
                                <input type="text" style="width: 100%;" id='forgotEmail' name='forgotEmail' size="30">
                            </td>                                            
                        </tr>
                    </table>
                    <div>
                        <input type="button" value="Reset Password" id="popup-btn" class="btn login-button" onclick="resetPassword();">
                        <br>
                        <span id="rpwmsg"></span>
                    </div>
                </form>                             
            </div>

        </div>
        <div id="blackout"></div>



    </body>
</html>



