
<script type="text/javascript">

    var CHECK_QUES_ON_EDIT = 0;
    var CHECK_QUES_AFTER_ENTERING_DATA = 0;
    var currentansId = '';

    function getQuestions() {

        var questionId = document.getElementById("qid").value;

        var URL = "<?php echo base_url('index.php/rest/getselquestion/id/'); ?>" + questionId;

        $.get(URL,
                function(data)
                {
                    var jsonData = jQuery.parseJSON(data);
                    var questionData = jsonData.questions;
                    var subjectNames = jsonData.subjects;
                    var answers = jsonData.answers;
                    var userNames = jsonData.users;
                    var userNamesans = jsonData.usersans;
                    var tags = jsonData.tags;
                    var isClosed = questionData[0].isclosed;

                    displayQuestions(questionData, subjectNames, answers, userNames, userNamesans, tags);
                    ceckLogin(isClosed);
                    loadEditBox();

                }).fail(function(data)
        {
            var jsonData = jQuery.parseJSON(data);
            if (jsonData.Status === "noResult") {

            }
        });
    }

    function displayQuestions(questionData, subjectNames, answers, userNames, userNamesans, tags) {

        var questionid = ' <form action="' + '<?php echo base_url('index.php/QuestionController/loadSelectedQues') ?>" id="questionIdForm" method="get" ><input type="hidden" name="questionId" value="';
        var questionidclose = '"></form>';

        var divNumofVotes = '<div class="votes-answers"> <div class="votes-box"> <div class="num-votes" id="numVotes';
        var divNumofVotes_close = '">';
        var divVotes = '</div><div class="votes-answers-font">votes</div></div>';
        var divNumofAns = '<div class="answers-box"><div class="votes-answers-font">';
        var divEndAns = '<br>answers</div></div></div>';

        var questionTitle = '<div class="question-box" ><div class="question-box2"><div class="question-font" id="';
        var questionIdClose = '" onclick="submitSelectedQuestion();" style="cursor: pointer;"><a id="questionTitle">';
        var questionSubject = '</a></div><div><div class="question-subject">';
        var questionTag = '';
        var questionDescription = '</div></div></div><br><div id="questionDescription" class="left-float">';
        var voteUp = '</div><br><br><div class="left-float"><input type="image" class="img-vote"  src="' + '<?php echo base_url('assets/img/up.png') ?>" onclick="vote(';
        var closeVoteUP = ',1,1)">';
        var voteDown = '<input type="image" class="img-vote" src="' + '<?php echo base_url('assets/img/down.png') ?>" onclick="vote(';
        var closeVoteDown = ',-1,1)"></div >';
        var questionAskedBy = '<div class="question-asked-by pull-right" style="margin-top: 10px;">';
        var answerBox = '<form id="answerForm"><textarea style="width: 95%; margin-top: 10px;" rows="8" cols="50" name="answer"></textarea><input type="hidden" name="uid" id="uid"><input type="hidden" name="ques" id="ques"> <br><br><input class ="btn btn-primary btn-large" type="button" value="Answer" id="answerBtn" onclick="postAnswer();"></form><div style="color: red" id="msg"></div> <br>';

        var closeQuestions = '</div><br><br><br><div><input type="button" value="Edit" class="btn login-button pull-right editbtn popup-link-1"><input type="button" value="Delete" class="btn login-button pull-right deletebtn" onclick="deleteQuestion();"><div id="closed"><input type="button" value="Close" class="btn login-button pull-right closebtn" onclick="closeQuestion();"></div></div><br>' + '<h4>Answers</h4><br>' + '</div>';
        var ansStart = '<div class="question-box">';
        var ansId = '<div id="ans';
        var answ = '">';
        var ansVoteup = '</div><div><div><input type="image" class="img-vote-ans"  src="' + '<?php echo base_url('assets/img/up.png') ?>" onclick="vote(';
        var anscloseVoteUP = ',1,0)"/>';
        var ansvoteDown = '<input type="image" class="img-vote-ans" src="' + '<?php echo base_url('assets/img/down.png') ?>" onclick="vote(';
        var anscloseVoteDown = ',-1,0)"/>';
        var closAns = '  </div><div style="min-height: 40px;"> <input type="button" value="Edit" onmouseover="getAnswerContent(this.id);" class="btn login-button pull-right editbtn popup-link-2" id="';
        var addIdEdit = '"><input type="button" onclick="deleteAnswer(this.id);" value="Delete" class="btn login-button pull-right deletebtn" id="';
        var ansAccept = '"><input type="button" onclick="AcceptAnswer(this.id);" value="Accept" class="btn login-button pull-right acceptbtn" id="';
        var andIdDel = '"</div></div></div></div></div>';

        var ans = "";
        if (answers !== "") {
            for (var i = 0; i < answers.length; i++) {

                if (answers[i].id === questionData[0].acceptedAnswer) {
                    ans = ans + '<div class="answers">' + divNumofVotes + answers[i].id + divNumofVotes_close + answers[i].votes + divVotes + '<div class="answers-box"><div class="votes-answers-font">' + '<i id="acptans" style="color: green;" class="fa  fa-check fa-2x"></i>' + '</div></div></div>' + ansStart + ansId + answers[i].id + answ + answers[i].answer + ansVoteup + answers[i].id + ',' + answers[i].answeredBy + anscloseVoteUP + ansvoteDown + answers[i].id + ',' + answers[i].answeredBy + anscloseVoteDown + questionAskedBy + userNamesans[i].name + " --- " + answers[i].answeredDate + closAns + answers[i].id + addIdEdit + answers[i].id + ansAccept + answers[i].id + andIdDel + '</div>';

                } else
                    ans = ans + '<div class="answers">' + divNumofVotes + answers[i].id + divNumofVotes_close + answers[i].votes + divVotes + '</div>' + ansStart + ansId + answers[i].id + answ + answers[i].answer + ansVoteup + answers[i].id + ',' + answers[i].answeredBy + anscloseVoteUP + ansvoteDown + answers[i].id + ',' + answers[i].answeredBy + anscloseVoteDown + questionAskedBy + userNamesans[i].name + " --- " + answers[i].answeredDate + closAns + answers[i].id + addIdEdit + answers[i].id + ansAccept + answers[i].id + andIdDel + '</div>';

            }
        }
        for (var i = 0; i < questionData.length; i++) {


            if (tags[i].indexOf(",") !== -1) {
                var res = tags[i].split(',');
                questionTag = '</div>';
                for (var j = 0; j < res.length; j++) {
                    questionTag = questionTag + '<div class="question-tag">' + res[j] + '</div>';
                }
                questionTag = questionTag + '<div>';
            }
            else {

                questionTag = '</div><div class="question-tag">' + tags[i];

            }
            var NumOfVotesAndAns = questionid + questionData[i].id + questionidclose + divNumofVotes + questionData[i].id + divNumofVotes_close + questionData[i].votes + divVotes + divNumofAns + countAns(questionData[i].answers) + divEndAns;

            var question = questionTitle + questionData[i].id + questionIdClose + questionData[i].title + questionSubject + subjectNames[+questionData[i].subject] + questionTag + questionDescription + questionData[i].description;
            var voting = voteUp + questionData[i].id + ',' + questionData[i].askedBy + closeVoteUP + voteDown + questionData[i].id + ',' + questionData[i].askedBy + closeVoteDown;
            var askedBy = questionAskedBy + userNames[i].name + " --- " + questionData[i].askedDate + closeQuestions + ans + answerBox;

            var questionSlot = NumOfVotesAndAns + question + voting + askedBy;
            var fragment = create(questionSlot);

            document.getElementById("questions").appendChild(fragment);

            document.getElementById("ques").value = document.getElementById("qid").value;
        }
    }

    function submitSelectedQuestion() {
        document.getElementById("questionIdForm").submit();
    }

    function countAns(answers) {
        if (answers !== "")
            return answers.split(",").length;
        else
            return '0';
    }

    function create(htmlStr) {
        var frag = document.createDocumentFragment();
        temp = document.createElement('div');

        temp.innerHTML = htmlStr;
        while (temp.firstChild) {
            frag.appendChild(temp.firstChild);
        }
        return frag;
    }

    function vote(id, owner, vote, table) {

        var tableparam;
        var uid = document.getElementById("uid").value;

        if (table === 1) {
            tableparam = "question";
        }
        else {
            tableparam = "answer";
        }

        var URL = "<?php echo base_url('index.php/rest/'); ?>" + tableparam;

        var postData = {
            "id": id,
            "owner": owner,
            "uid": uid,
            "votes": vote
        };

        $.post(URL,
                postData,
                function(data)
                {
                    var jsonData = jQuery.parseJSON(data);

                    if (jsonData.Status === true) {
                        var currentVote = document.getElementById('numVotes' + id).innerHTML;
                        var newVote = +currentVote + +vote;
                        document.getElementById('numVotes' + id).innerHTML = newVote;
                    } else
                        alert(jsonData.Status);


                }).fail(function(data) {
            alert("ERROR!\n" + data);
        });
    }

    function postAnswer() {
        var formData = $("#answerForm").serializeArray();
        var URL = "<?php echo base_url('index.php/rest/answers'); ?>";

        $.post(URL,
                formData,
                function(data)
                {
                    var jsonData = jQuery.parseJSON(data);
                    if (jsonData.Status === "success") {
                        location.reload();
                    }
                    else {
                        alert("not");
                        document.getElementById('msg').innerHTML = jsonData.Status;
                    }

                }).fail(function(data)
        {
            var jsonData = jQuery.parseJSON(data);
            if (jsonData.Status === "noResult") {

            }
        });
    }

    function closeQuestion() {
        var confirmClose = confirm("Are you sure you want to close the question?");

        if (confirmClose) {

            var URL = "<?php echo base_url('index.php/rest/closequestion'); ?>";
            var qid = document.getElementById("qid").value;
            var postData = {
                "id": qid
            };

            $.post(URL,
                    postData,
                    function(data)
                    {
                        if (data === true)
                            alert("Successfully closed the question");
                        else
                            alert(data);

                    }).fail(function(data) {

                alert("ERROR!\n" + data);
            });
        }
    }

    function AcceptAnswer(andId) {

        var URL = "<?php echo base_url('index.php/rest/acceptanswer'); ?>";
        var qid = document.getElementById("qid").value;
        var postData = {
            "id": qid,
            "ansid": andId
        };

        $.post(URL,
                postData,
                function(data)
                {
                    alert(data);

                }).fail(function(data) {

            alert("ERROR!\n" + data);
        });
    }

    function deleteQuestion() {

        var confirmDel = confirm("Are you sure you want to delete?");

        if (confirmDel) {

            var URL = "<?php echo base_url('index.php/rest/deletequestion'); ?>";
            var qid = document.getElementById("qid").value;
            var uid = document.getElementById("uid").value;
            var postData = {
                "id": qid,
                "uid": uid
            };

            $.post(URL,
                    postData,
                    function(data)
                    {
                        if (data) {
                            alert("Successfully deleted the question");
                            window.location = "<?php echo base_url('index.php/questioncontroller/questionview') ?>";
                        } else
                            alert(data);

                    }).fail(function(data) {

                alert("ERROR!\n" + data);
            });
        }
    }

    function deleteAnswer(id) {
        var confirmDel = confirm("Are you sure you want to delete?");

        if (confirmDel) {
            var uid = document.getElementById("uid").value;
            var URL = "<?php echo base_url('index.php/rest/deleteanswer'); ?>";

            var postData = {
                "id": id,
                "uid": uid
            };

            $.post(URL,
                    postData,
                    function(data)
                    {
                        if (data === true) {
                            alert("Successfully deleted the answer");
                            location.reload();
                        }
                        else
                            alert(data);

                    }).fail(function(data)
            {

                alert("ERROR!\n" + data);
            });
        }
    }

    function ceckLogin(isClosed) {
        var grp = document.getElementById("grp").value;
        if (isClosed === '1') {
            document.getElementById('questionTitle').innerHTML = document.getElementById('questionTitle').innerHTML + ' [Closed]';
            document.getElementById('closed').style.display = "none";
            document.getElementById("answerBtn").disabled = true;

            var editbtnQus = document.getElementsByClassName('btn login-button pull-right editbtn popup-link-1');
            for (var i = 0; i < editbtnQus.length; i++) {
                editbtnQus[i].style.display = "none";
            }
        }

        if (grp !== '') {
            document.getElementById("uid").value = document.getElementById("userId").value;
        } else {
            var deletebtn = document.getElementsByClassName('btn login-button pull-right deletebtn');
            var editbtnQus = document.getElementsByClassName('btn login-button pull-right editbtn popup-link-1');
            var editbtnAns = document.getElementsByClassName('btn login-button pull-right editbtn popup-link-2');

            for (var i = 0; i < deletebtn.length; i++) {
                deletebtn[i].style.display = "none";
            }

            for (var i = 0; i < editbtnQus.length; i++) {
                editbtnQus[i].style.display = "none";
            }
            for (var i = 0; i < editbtnAns.length; i++) {
                editbtnAns[i].style.display = "none";
            }

            document.getElementById('closed').style.display = "none";
        }

        if (grp === "2") {
            document.getElementById("uid").value = document.getElementById("userId").value;
        }
        else {
            document.getElementById("answerBtn").disabled = true;

            if (grp === '' || grp === '3') {
                document.getElementById('closed').style.display = "none";

                var voteButtonsques = document.getElementsByClassName('img-vote');
                for (var i = 0; i < voteButtonsques.length; i++) {
                    voteButtonsques[i].disabled = true;
                }

                var voteButtonsans = document.getElementsByClassName('img-vote-ans');
                for (var i = 0; i < voteButtonsans.length; i++) {
                    voteButtonsans[i].disabled = true;
                }

                var editbtnQus = document.getElementsByClassName('btn login-button pull-right editbtn popup-link-1');
                var editbtnAns = document.getElementsByClassName('btn login-button pull-right editbtn popup-link-2');
                var acceptAns = document.getElementsByClassName('btn login-button pull-right acceptbtn');

                for (var i = 0; i < editbtnQus.length; i++) {
                    editbtnQus[i].style.display = "none";
                }
                for (var i = 0; i < editbtnAns.length; i++) {
                    editbtnAns[i].style.display = "none";
                }
                for (var i = 0; i < acceptAns.length; i++) {
                    acceptAns[i].style.display = "none";
                }
            }
        }
    }

    function editQuestion() {
        var id = document.getElementById("qid").value;
        var newTitle = document.getElementById("newTitle").value;
        var newDescription = document.getElementById("newDescription").value;

        var URL = "<?php echo base_url('index.php/rest/editquestion'); ?>";

        var postData = {
            "id": id,
            "title": newTitle,
            "description": newDescription
        };

        $.post(URL,
                postData,
                function(data)
                {
                    document.getElementById('qmsg').innerHTML = data;


                }).fail(function(data) {
            alert("ERROR!\n" + data);
        });
    }

    function getAnswerContent(id) {
        currentansId = 'ans' + id;
    }

    function editAnswer(id) {

        var newAnswer = document.getElementById('newAnswer').value;
        var id = id.substring(3, id.length);

        var URL = "<?php echo base_url('index.php/rest/editanswer') ?>";

        var postData = {
            "id": id,
            "answer": newAnswer
        };

        $.post(URL,
                postData,
                function(data)
                {
                    if (data.indexOf("Successfully edited the answer.") != -1)
                        location.reload();
                    document.getElementById('amsg').innerHTML = data;

                }).fail(function(data) {
            alert("ERROR!\n" + data);
        });
    }

    function getQuestionContent() {
        var question = new Array();
        question[0] = document.getElementById("questionTitle").innerHTML;
        question[1] = document.getElementById("questionDescription").innerHTML;
        return question;
    }

    function loadEditBox() {
        var boxWidth = 900;
        function centerBox() {
            /* Preliminary information */
            var winWidth = $(window).width();
            var winHeight = $(document).height();
            var scrollPos = $(window).scrollTop();
            /* auto scroll bug */
            /* Calculate positions */
            var disWidth = (winWidth - boxWidth) / 2;
            var disHeight = scrollPos + 150;
            /* Move stuff about */
            $('.popup-box').css({'width': boxWidth + 'px', 'left': disWidth + 'px', 'top': disHeight + 'px'});
            $('#blackout').css({'width': winWidth + 'px', 'height': winHeight + 'px'});
            return false;
        }
        $(window).resize(centerBox);
        $(window).scroll(centerBox);
        centerBox();
        $('[class*=popup-link]').click(function(e) {
            /* Prevent default actions */
            e.preventDefault();
            e.stopPropagation();
            /* Get the id (the number appended to the end of the classes) */
            var name = $(this).attr('class');
            var id = name[name.length - 1];
            var scrollPos = $(window).scrollTop();

            /* Show the correct popup box, show the blackout and disable scrolling */
            $('#popup-box-' + id).show();
            $('#blackout').show();
            $('html,body').css('overflow', 'hidden');
            /* Fixes a bug in Firefox */
            $('html').scrollTop(scrollPos);
            if (id === '1') {
                var content = getQuestionContent();
                document.getElementById('qmsg').innerHTML = '';
                document.getElementById("newTitle").value = content[0];
                document.getElementById("newDescription").value = content[1];
            }
            else {
                document.getElementById('amsg').innerHTML = '';
                document.getElementById("newAnswer").value = document.getElementById(currentansId).innerHTML;
            }
        });
        $('[class*=popup-box]').click(function(e) {
            /* Stop the link working normally on click if it's linked to a popup */
            e.stopPropagation();
        });
        $('html').click(function() {
            var scrollPos = $(window).scrollTop();
            /* Hide the popup and blackout when clicking outside the popup */
            $('[id^=popup-box-]').hide();
            $('#blackout').hide();
            $("html,body").css("overflow", "auto");
            $('html').scrollTop(scrollPos);
        });
        $('.close').click(function() {

            var scrollPos = $(window).scrollTop();
            /* Similarly, hide the popup and blackout when the user clicks close */
            $('[id^=popup-box-]').hide();
            $('#blackout').hide();
            $("html,body").css("overflow", "auto");
            $('html').scrollTop(scrollPos);

        });
    }
</script>

<body onload="getQuestions();">

    <div class="content">
        <div class="container">
            <div class="row">
                <div class="span6 offset3">
                    <!--<h3 class="widget-header"> Questions</h3>-->
                    <div class="widget-body">
                        <input type="hidden" id="qid" value=<?php echo '"' . $subject['question'] . '"'; ?> >
                        <input type="hidden" id="grp" value=<?php echo '"' . $subject['grp'] . '"'; ?> >
                        <div id='questions' style="margin-left: 40px;"></div>
                        <!--<a class="popup-link-1">Change Password</a>-->
                        <div class="popup-box editPopup" id="popup-box-1">
                            <div class="close">X</div>
                            <div class="top">
                                <h2>Edit Question</h2>
                            </div>
                            <div class="bottom">
                                <form> 
                                    <table style="text-align:left; width: 100%;">
                                        <tr>
                                            <td style="width: 10%;">Title</td>
                                            <td>
                                                <input style="width: 100%;" type="text" id='newTitle' name='newTitle' >
                                            </td>
                                        <tr>
                                            <td>Description</td>
                                            <td><textarea cols="300" rows="4" id='newDescription' name='newDescription' style="width: 100%;"></textarea>

                                            </td>                                            
                                        </tr>
                                    </table>                                                                            
                                    <input type="button" value="Edit" id="popup-btn" class="btn login-button" onclick="editQuestion();">
                                    <br>
                                    <span id="qmsg"></span>
                            </div>
                            </form>                             
                        </div>

                        <div class="popup-box editPopup" id="popup-box-2">
                            <div class="close">X</div>
                            <div class="top">
                                <h2>Edit Answer</h2>
                            </div>
                            <div class="bottom">
                                <form> 
                                    <table style="text-align:left; width: 100%;">
                                        <tr>
                                            <td>Answer</td>
                                            <td>
                                                <textarea cols="300" rows="4" id='newAnswer' name='newAnswer' style="width: 100%;"></textarea>
                                            </td>                                            
                                        </tr>
                                    </table>                                                                            
                                    <input type="button" value="Edit" id="popup-btn" class="btn login-button" onclick="editAnswer(currentansId);">
                                    <br>
                                    <span id="amsg"></span>
                                </form> 
                            </div>                                                         
                        </div>

                    </div>
                    <div id="blackout"></div>



                </div>
            </div>
        </div>
    </div> 

</div>
</body>