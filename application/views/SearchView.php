<html>
    <script type="text/javascript">

        var prevbtn = '<input type="button" class="btn pg-btn login-button" ';
        var searchType = 'onclick="';
        var prevbtn_close = '(this.id);" id="' + FIRST_PAGE + '" value="<<">';
        var pageId = '<input type="button" class="btn login-button pg-btn" id="';
        var btnOnclick = '" onclick="';
        var btnOnclick_close = '(this.id);"';
        var pageNo = ' value="';
        var pageNo_close = '">';

        var lastbtn = '<input type="button" class="btn login-button pg-btn"'
        var lastbtn_close = '(this.id);" id="' + numOfPage + '" value=">>">';

        var numOfPage;
        var MAX_PAGE_NUMBERS = 5;
        var FIRST_PAGE = 1;
        var BASIC_SEARCH_TYPE = 'getSearch';
        var ADVANCED_SEARCH_TYPE = 'advanceSearch';

        //Get the search results
        function getSearch(pageNum) {
            document.getElementById("arrowUp").style.display = "none";
            var searchTerm = document.getElementById("searchTerm").value;
            var URL = "<?php echo base_url('index.php/rest/search'); ?>" + "/pg/" + pageNum;

            searchTerm = {
                "searchTerm": searchTerm
            };


            $.get(URL,
                    searchTerm,
                    function(data)
                    {
                        var jsonData = jQuery.parseJSON(data);
                        $('#pages').empty();
                        if (jsonData.questions === false) {
                            var fragment = create("Your search did not match any question. Please try using, <ul><li>Different keywords</li><li>More general keywords</li></ul>");
                            document.getElementById("questions").appendChild(fragment);

                        } else {
                            var subjectNames = jsonData.subjects;
                            var userNames = jsonData.users;
                            var tags = jsonData.tags;
                            numOfPage = jsonData.numOfPage;

                            displayQuestions(jsonData.questions, subjectNames, userNames, tags);
                            if (numOfPage > 1)
                                pagination(pageNum, 0);
                        }
                    }).fail(function(data)
            {
                var jsonData = jQuery.parseJSON(data);
                if (jsonData.Status === "noResult") {

                }
            });
        }

        //Get the advanced search results
        function advanceSearch(pageNum) {
            document.getElementById("searchTerm").value = "";
            document.getElementById("questions").innerHTML = "";

            var searchTerms = $("#advanceSearchForm").serializeArray();
            var URL = "<?php echo base_url('index.php/rest/advancesearch'); ?>" + "/pg/" + pageNum;

            $.get(URL,
                    searchTerms,
                    function(data)
                    {
                        var jsonData = jQuery.parseJSON(data);
                        $('#pages').empty();

                        if (jsonData.questions === false) {
                            var fragment = create("Your search did not match any question. Please try using, <ul><li>Different keywords</li><li>More general keywords</li></ul>");
                            document.getElementById("questions").appendChild(fragment);

                        } else {
                            var subjectNames = jsonData.subjects;
                            var userNames = jsonData.users;
                            var tags = jsonData.tags;
                            numOfPage = jsonData.numOfPage;
                            displayQuestions(jsonData.questions, subjectNames, userNames, tags);

                            if (numOfPage > 1)
                                pagination(pageNum, 1);
                        }

                    }).fail(function(data)
            {
                var jsonData = jQuery.parseJSON(data);
                if (jsonData.Status === "noResult") {

                }
            });
        }
        
        //Pagination for the pages < 5
        function pagination(pageNum, typsOfSearch) {

            var type;
            if (+typsOfSearch === 0)
                type = BASIC_SEARCH_TYPE;
            else
                type = ADVANCED_SEARCH_TYPE;

            var pages = '';
            if (numOfPage <= MAX_PAGE_NUMBERS) {

                for (var j = 1; j <= numOfPage; j++) {
                    pages = pages + pageId + j + btnOnclick + type + btnOnclick_close + pageNo + j + " " + pageNo_close;
                }
                var fragment = create(pages);
                document.getElementById("pages").appendChild(fragment);
            }
            else {
                paginationForLot(pageNum, typsOfSearch);
            }
        }

        //Pagination for the pages > 5
        function paginationForLot(pageNum, typsOfSearch) {
            var type;
            if (+typsOfSearch === 0)
                type = BASIC_SEARCH_TYPE;
            else
                type = ADVANCED_SEARCH_TYPE;

            if (pageNum < MAX_PAGE_NUMBERS - 2) {

                var start = 1;
                var end = MAX_PAGE_NUMBERS;

                for (var j = start; j <= end; j++) {
                    pages = pages + pageId + j + btnOnclick + type + btnOnclick_close + pageNo + j + " " + pageNo_close;
                }
                pages = pages + lastbtn + searchType + type + lastbtn_close;

                var fragment = create(pages);
                document.getElementById("pages").appendChild(fragment);
            }
            else {
                var start = +pageNum - 2;
                var end = +pageNum + 2;

                if (end > numOfPage)
                    end = numOfPage;

                for (var j = start; j <= end; j++) {
                    pages = pages + pageId + j + btnOnclick + type + btnOnclick_close + pageNo + j + " " + pageNo_close;
                }
                pages = prevbtn + searchType + type + prevbtn_close + pages + lastbtn + searchType + type + lastbtn_close;

                var fragment = create(pages);
                document.getElementById("pages").appendChild(fragment);
            }


        }
        
        //Display questions dynamically
        function displayQuestions(questionData, subjectNames, userNames, tags) {

            $('#questions').empty();
            var url = "<?php echo base_url('index.php/QuestionController/loadSelectedQues'); ?>";
            var questionid = ' <li style = "display: block;"><div><form action="' + url + '" id="questionIdForm';
            var formId = '" method="get" ><input type="hidden" name="questionId" value="';
            var questionidclose = '"></form>';

            var divNumofVotes = '<div class="votes-answers"> <div class="votes-box"> <div class="num-votes">';
            var divVotes = '</div><div class="votes-answers-font">votes</div></div>';
            var divNumofAns = '<div class="answers-box"><div class="votes-answers-font">';
            var divEndAns = '<br>answers</div></div></div>';

            var questionTitle = '<div class="question-box" ><div class="question-box2"><div class="question-font" id="';
            var questionIdClose = '" onclick="submitSelectedQuestion(this.id);" style="cursor: pointer;"><a>';
            var questionSubject = '</a></div><div><div class="question-subject">';
            var questionTag = '';
            var questionDescription = '</div></div></div><br>';

            var questionAskedBy = '<br><div class="question-asked-by">';
            var closeQuestions = '</div><hr></div></div></div></li>';


            for (var i = 0; i < questionData.length; i++) {

                if (tags[i].indexOf(",") !== -1) {
                    var res = tags[i].split(',');
                    questionTag = '</div>';
                    for (var j = 0; j < res.length; j++) {
                        questionTag = questionTag + '<div class="question-tag">' + res[j] + '</div>';
                    }
                    questionTag = questionTag + '<div>';
                }
                else {
                    questionTag = '</div><div class="question-tag">' + tags[i];
                }

                var NumOfVotesAndAns = questionid + questionData[i].id + formId + questionData[i].id + questionidclose + divNumofVotes + questionData[i].votes + divVotes + divNumofAns + countAns(questionData[i].answers) + divEndAns;

                var question = questionTitle + questionData[i].id + questionIdClose + questionData[i].title + questionSubject + subjectNames[+questionData[i].subject] + questionTag + questionDescription + questionData[i].description;

                var askedBy = questionAskedBy + userNames[i].name + " --- " + questionData[i].askedDate + closeQuestions;

                var questionSlot = NumOfVotesAndAns + question + askedBy;
                var fragment = create(questionSlot);

                document.getElementById("questions").appendChild(fragment);
            }
        }

        //Get the selected question
        function submitSelectedQuestion(id) {
            var id = "questionIdForm" + id;
            document.getElementById(id).submit();
        }

        //Count the total number of answers
        function countAns(answers) {
            if (answers !== "")
                return answers.split(",").length;
            else
                return '0';
        }

        function create(htmlStr) {
            var frag = document.createDocumentFragment();
            temp = document.createElement('div');

            temp.innerHTML = htmlStr;
            while (temp.firstChild) {
                frag.appendChild(temp.firstChild);
            }
            return frag;
        }

        function showAdSearch() {

            if (document.getElementById("arrowUp").style.display === "none") {
                document.getElementById("arrowDown").style.display = "none";
                document.getElementById("advanceSearchFormTbl").style.display = "none";
                document.getElementById("arrowUp").style.display = "inherit";
            }
            else {
                document.getElementById("arrowDown").style.display = "inherit";
                document.getElementById("advanceSearchFormTbl").style.display = "inherit";
                document.getElementById("arrowUp").style.display = "none";
            }
        }

    </script>
    <body onload="getSearch(1, 0);">
        <div class="content">
            <div class="container">               
                <div class="row">
                    <div class="span6 offset3">
                        <div id="advance-search">

                            <table style="border-spacing: 0px 0px;">
                                <tr>
                                    <td><h4> Advanced Search </h4></td>
                                    <td>
                                        <input type="image" id='arrowDown' onClick='showAdSearch();' src="<?php echo base_url('assets/img/arrowDown.png'); ?>"> 

                                        <input type="image" id='arrowUp' onClick='showAdSearch();' src="<?php echo base_url('assets/img/arrowUp.png'); ?>"> 
                                    </td>
                                </tr>
                            </table>

                            <table id='advanceSearchFormTbl'>
                                <form id='advanceSearchForm'>
                                    <tr> 
                                        <td>Questions Containing</td>
                                        <td><input type="text" name="advancesearchTermTitle" placeholder="Search in title"></td>
                                        <td><input type="text" name="advancesearchTermDesc" placeholder="Search in description"></td>  
                                        <td>in</td>
                                        <td>
                                            <?php
                                            echo "<select name='selectedSubject' >";

                                            $co = 0;

                                            echo '<option value="' . $co . '">Select Subject</option>';
                                            foreach ($subject as $item) {
                                                $co = $co + 1;
                                                echo '<option value="' . $co . '" >' . $item . '</option>';
                                            }
                                            ?>
                                        </td>
                                        <td><input type="text" name="tag" placeholder="Tag"><br></td>
                                    </tr>
                                    <tr>
                                        <td>Asked by</td>
                                        <td><input type="text" name="askedBy" placeholder="Asked By"> </td>
                                        <td></td>
                                        <td>Sort by</td>
                                        <td>
                                            <select name="sortBy">
                                                <option value="votes">Votes</option>
                                                <option value="askedDate">Date</option>
                                            </select>

                                            <select name="ordertBy" >
                                                <option value="desc">Descending</option>
                                                <option value="asc">Ascending</option>                                                
                                            </select>
                                        </td>
                                        <td>
                                            <select name="type" >
                                                <option value="all">All Questions</option>
                                                <option value="ans">Answered Questions</option>  
                                                <option value="unans">Unanswered Questions</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><input type="button" value="Search" class="btn btn-primary btn-large" onclick='advanceSearch(1);'></td>
                                    </tr>
                                </form>
                            </table>
                        </div>

                        <h4 class="widget-header" style="margin-left: 10px;">Search Results</h4> 

                        <input type="hidden" id="searchTerm" value= <?php echo '"' . $searchTerm . '"'; ?>>

                        <br>
                        <div class="widget-body">

                            <ul id='questions'> </ul>
                            <div id="pages" style="text-align: center"></div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>

