<?php

class UserModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('authlib');
    }

    //Register user
    function register($name, $email, $pwd, $dob, $favouriteSubject, $userGroup) {
        // is username unique?
        $res = $this->db->get_where('user', array('email' => $email));
        if ($res->num_rows() > 0) {
            return 'Already Registered';
        }
        // username is unique     
        $hashpwd = sha1($pwd);
        $joinedDate = date("Y-m-d H:i:s");
        $data = array('name' => $name, 'email' => $email, 'password' => $hashpwd, 'dob' => $dob, 'favouriteSubject' => $favouriteSubject, 'userGroup' => $userGroup, 'joinedDate' => $joinedDate);
        $this->db->insert('user', $data);
        return true; // no error message because all is ok
    }

    //Login user
    function login($email, $pwd) {

        $this->db->where(array('email' => $email, 'password' => sha1($pwd)));
        $res = $this->db->get('user', array('email'));
        if ($res->num_rows() != 1) { // should be only ONE matching row
            return 'Incorrect credentiols/ Not registered';
        }

        $session_id = $this->session->userdata('session_id');
        // remember current login
        $row = $res->row_array();
        $this->db->insert('logins', array('email' => $row['email'], 'session_id' => $session_id));
        return true;
    }

    //Change user details
    function change($name, $dob, $favouriteSubject) {
        $id = $this->getloggedUserID();

        $data = array('name' => $name, 'dob' => $dob, 'favouriteSubject' => $favouriteSubject);

        $this->db->where('id', $id);
        $this->db->update('user', $data);
        return true;
    }

    //Change password
    public function changePw($id, $pwd) {

        $hashpwd = sha1($pwd);
        $data = array('password' => $hashpwd);

        $this->db->where('id', $id);
        $this->db->update('user', $data);
        return true;
    }

    //Get the id according to email
    function getUserID($email) {

        $this->db->select('id');
        $q = $this->db->get_where('user', array('email' => $email));
        $ans = $q->row(0);

        return $ans;
    }

    //Check whether the user is logged in
    function is_loggedin() {
        $session_id = $this->session->userdata('session_id');
        $res = $this->db->get_where('logins', array('session_id' => $session_id));
        if ($res->num_rows() == 1) {
            $row = $res->row_array();
            return $row['email']; //Return the email of the logged in user
        } else {
            return false;
        }
    }

    //Get the number of pages for tutors/ students page
    function numOfPages($type) {
        $query = $this->db->get_where('user', array('userGroup' => $type));
        $count = $query->num_rows();
        $numofPages = (int) ($count / USERS_PER_PAGE);
        if ($count % USERS_PER_PAGE != 0)
            $numofPages = $numofPages + 1;
        return $numofPages;
    }

    //Get the students/ tutors details
    public function getUsers($pageNumber, $type) {
        $pageNumber = (int) $pageNumber;
        $startingItem = ($pageNumber - 1) * USERS_PER_PAGE;

        $this->db->select('id,name, favouriteSubject, joinedDate, reputation');
        $this->db->where(array('userGroup' => $type));
        $this->db->order_by('reputation', 'desc');

        $q = $this->db->get('user', USERS_PER_PAGE, $startingItem);

        $row = array();

        if ($q->num_rows() > 0) {
            for ($j = 0; $j < $q->num_rows(); $j++) {
                $row[$j] = $q->row($j);
            }
            return $row;
        }
    }

    //Reocord voted user
    public function recordVotedUser($uid, $id, $voteFor, $owner) {
        if ($owner === $uid) //Owener of the question/answer cannot vote again
            return "You cannot vote for your own questions/ answers";

        if ($voteFor == QUESTION)
            $voteColumn = "votedQuestions";
        else
            $voteColumn = "votedAnswers";

        $this->db->select($voteColumn);
        $query = $this->db->get_where('user', array('id' => $uid));
        $votes = "";

        foreach ($query->result_array() as $row) {
            $votes = $row[$voteColumn];
        }

        if (strpos($votes, $id) !== false) //User can't vote if he is already voted
            return "Already voted";

        if ($votes == "")
            $newVotes = $id;
        else
            $newVotes = $votes . ',' . $id;

        $data = array($voteColumn => $newVotes);

        $this->db->where('id', $uid);
        $this->db->update('user', $data);
        return TRUE;
    }

    //Calculate the reputation
    public function calcReputation($owner, $votes) {

        $numOfVotes = 0;

        $this->db->select('reputation');
        $query = $this->db->get_where('user', array('id' => $owner));

        foreach ($query->result_array() as $row) {
            $numOfVotes = $row['reputation'];
        }

        $votes = $numOfVotes + $votes;
        $data = array('reputation' => $votes);

        $this->db->where('id', $owner);
        $this->db->update('user', $data);
        return true;
    }

    //Get user details for user profile view
    public function getUserDetails($args) {
        $userId = $args['id'];
        $row = array();

        $this->db->select('email, name, dob, favouriteSubject, joinedDate, reputation, userGroup');
        $q = $this->db->get_where('user', array('id' => $userId));

        if ($q->num_rows() > 0) {

            for ($j = 0; $j < 1; $j++) {
                $row[$j] = $q->row($j);
            }
            return $row;
        }
        else
            return false;
    }

    //Delete user
    public function deleteUser($id) {
        $data = array('email' => '', 'password' => '', 'dob' => '', 'favouriteSubject' => '', 'userGroup' => '', 'joinedDate' => '');

        $this->db->where('id', $id);
        $this->db->update('user', $data);
        return true;
    }

    //Get the user group
    public function checkUserGroup($user) {
        $this->db->select('userGroup');
        $q = $this->db->get_where('user', array('email' => $user));
        $ans = $q->row(0);

        return $ans;
    }

    //Get the id of logged in user
    function getloggedUserID() {

        $email = $this->is_loggedin();
        if ($email !== false) {
            $this->db->select('id');
            $q = $this->db->get_where('user', array('email' => $email));
            $ans = $q->row(0);
            $ans = (Array) $ans;

            return $ans['id'];
        }
        return false;
    }

    //Validate the email
    function validateEmail($email) {
        $this->db->select('id');
        $q = $this->db->get_where('user', array('email' => $email));

        if ($q->num_rows() < 1) {
            return false;
        } else {
            $newPw = $this->generatePassword();
            $hashpwd = sha1($newPw);
            $data = array('password' => $hashpwd);

            $this->db->where('email', $email);
            if ($this->db->update('user', $data))
                return $newPw;
        }
    }

    //Generate a password with random alphanumeric values
    function generatePassword() {
        return random_string('alnum', 6);
    }

}

?>
