<?php

class QuestionModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('authlib');
    }

    //Ask question
    public function askQuestion($title, $description, $subject, $tags) {

        $tags = strtolower(trim($tags));
        $tagWords = explode(' ', $tags);
        $cnt = 0;
        $tw;
        foreach ($tagWords as $keyword) { //Match the tags with tag ids and insert
            $tag = '';
            $this->db->select('id');
            $query = $this->db->get_where('tag', array('name' => $keyword));

            foreach ($query->result_array() as $row) {
                $tag = $row['id'];
            }

            if ($tag == '') {

                $data = array('name' => $keyword);
                $this->db->insert('tag', $data);
                $tw[$cnt] = $this->db->insert_id();
                $cnt++;
            } else {
                $tw[$cnt] = $tag;
                $cnt++;
            }
        }

        $tags = '';
        for ($j = 0; $j < count($tw); $j++) {
            if ($j == 0)
                $tags = strval($tw[$j]);
            else
                $tags = $tags . ',' . strval($tw[$j]);
        }

        $askedBy = $this->getUser(); //Get the asked user id
        $askedDate = date("Y-m-d H:i:s");
        $data = array('title' => $title, 'description' => $description, 'askedBy' => $askedBy, 'askedDate' => $askedDate, 'subject' => $subject, 'tags' => $tags);
        $this->db->insert('question', $data); //Insert all the details

        return true;
    }

    //Get the tag IDs
    public function getTag($tags) {

        for ($i = 0; $i < count($tags); $i++) {

            if (strlen($tags[$i]) > 1) {
                $tempTag = '';
                $words = explode(',', $tags[$i]);
                $firstKeyWord = true;

                for ($c = 0; $c < count($words); $c++) {
                    $id = $words[$c];
                    $this->db->select('name');
                    $q = $this->db->get_where('tag', array('id' => $id));
                    $tempKey = (Array) $q->row(0);
                    if ($firstKeyWord)
                        $tempTag = $tempKey['name'];
                    else
                        $tempTag = $tempTag . ',' . $tempKey['name'];
                    $firstKeyWord = false;
                }
                $tags[$i] = $tempTag;
            }else {
                $id = $tags[$i];

                $this->db->select('name');
                $q = $this->db->get_where('tag', array('id' => $id));

                $tempKey = (Array) $q->row(0);

                $tags[$i] = $tempKey['name'];
            }
        }
        return $tags;
    }

    //Get user's id
    function getUser() {
        $session_id = $this->session->userdata('session_id'); //Get the session id of the user
        $email = " ";
        $res1 = $this->db->get_where('logins', array('session_id' => $session_id)); //Get the email of logged in user
        if ($res1->num_rows() == 1) {
            $row = $res1->row_array();
            $email = $row['email'];
        } else {
            return false;
        }

        $res2 = $this->db->get_where('user', array('email' => $email)); //Get the user id of logged in uer
        if ($res2->num_rows() == 1) {
            $row = $res2->row_array();
            return $row['id'];
        } else {
            return false;
        }
    }

    //Get all the subjects
    function getSubjects() {
        $this->db->select('name');
        $q = $this->db->get('subject');
        $tt = array();
        $count = 0;

        if ($q->num_rows() > 0) {

            foreach ($q->result_array() as $row) {
                $count++;
                $tt [$count] = $row['name'];
            }
            return $tt;
        }
    }

    //Get all the subject descriptions
    function getSubjectDesc() {
        $this->db->select('description');
        $q = $this->db->get('subject');
        $tt = array();
        $count = 0;

        if ($q->num_rows() > 0) {

            foreach ($q->result_array() as $row) {
                $count++;
                $tt [$count] = $row['description'];
            }
            return $tt;
        }
    }

    //Get the tatal number of pages according to the total number of questions
    function numOfPages() {

        $count = $this->db->count_all('question');
        $numofPages = (int) ($count / QUESTIONS_PER_PAGE);
        if ($count % QUESTIONS_PER_PAGE != 0)
            $numofPages = $numofPages + 1;
        return $numofPages;
    }

    //Load the questions according to tha page number
    function loadQuestions($pageNumber) {

        $pageNumber = (int) $pageNumber;
        $startingItem = ($pageNumber - 1) * QUESTIONS_PER_PAGE;
        $this->db->select('id,title, description, answers, askedBy, askedDate, tags, subject, votes');
        $this->db->order_by('votes', 'desc');
        //Get question limiting from the maximum questions per page from the starting page
        $q = $this->db->get('question', QUESTIONS_PER_PAGE, $startingItem);

        $row = array();

        if ($q->num_rows() > 0) {
            for ($j = 0; $j < $q->num_rows(); $j++) {
                $row[$j] = $q->row($j);
            }
            return $row; //Return the questions
        }
    }

    //Load selected question once user clicks on a title of a question
    function loadSelQuestion($args) {

        $id = $args['id'];
        $this->db->select('id,title, description, answers, askedBy, askedDate, tags, subject, votes, isclosed, acceptedAnswer');
        $q = $this->db->get_where('question', array('id' => $id));
        $row = array();

        if ($q->num_rows() > 0) {
            for ($j = 0; $j < 1; $j++) {
                $row[$j] = $q->row($j);
            }
            return $row; // Return the question details as an array
        }
    }

    //Get the user names for a given user IDs
    function getUsers($users) {

        for ($i = 0; $i < count($users); $i++) {
            $id = $users[$i];
            $this->db->select('name');
            $q = $this->db->get_where('user', array('id' => $id));
            $users[$i] = $q->row($i);
        }
        return $users; //Return user names
    }

    //Get the answers for a given set of answer IDs
    function getAnswers($answers) {

        $ans = explode(',', $answers);

        for ($i = 0; $i < count($ans); $i++) {
            $id = $ans[$i];
            $this->db->select('id,answer, answeredBy, answeredDate, votes');
            $q = $this->db->get_where('answer', array('id' => $id)); //Match the IDs with names
            $ans[$i] = $q->row($i);
        }

        return $ans; //Return answers
    }

    //Add answer ID to the question
    public function addAnsId($qid, $ansId) {
        $this->db->select('answers');
        $query = $this->db->get_where('question', array('id' => $qid));
        $answers = "";

        foreach ($query->result_array() as $row) {
            $answers = $row['answers'];
        }
        if ($answers == "")
            $newAnswers = $ansId;
        else
            $newAnswers = $answers . ',' . $ansId; //Create comma seperated ID list of answers

        $data = array('answers' => $newAnswers);

        $this->db->where('id', $qid);
        $this->db->update('question', $data);
        return true;
    }

    //Delete question
    public function deleteQuestion($id, $uid) {

        $this->db->select('userGroup');
        $query = $this->db->get_where('user', array('id' => $uid));
        $userGroup = "";

        foreach ($query->result_array() as $row) {
            $userGroup = $row['userGroup'];
        }
        $userGroup = (int) $userGroup;

        $this->db->select('askedBy');
        $query = $this->db->get_where('question', array('id' => $id));
        $askedBy = "";

        foreach ($query->result_array() as $row) {
            $askedBy = $row['askedBy'];
        }

        //only the user who asked the question/ admin can delete a question
        if ($askedBy === $uid || $userGroup === ADMIN_GROUP) {

            //Delete the id from question table
            $questionId = array('id' => $id);
            $ms = $this->db->delete('question', $questionId);

            //Delete the id from answer table
            $answers = array('question' => $id);
            $delete = $this->db->delete('answer', $answers);

            return TRUE;
        } else if ($askedBy !== $uid) {
            return "You can ONLY delete the questions asked by you";
        }
    }

    //Delete an answer
    public function deleteAnswer($id, $uid) {

        $this->db->select('userGroup');
        $query = $this->db->get_where('user', array('id' => $uid));
        $userGroup = "";

        //Check user group tp see whether the user is an admin
        foreach ($query->result_array() as $row) {
            $userGroup = $row['userGroup'];
        }
        $userGroup = (int) $userGroup;

        //Check whether the delete requested user is the one who answered
        $this->db->select('answeredBy');
        $query = $this->db->get_where('answer', array('id' => $id));
        $answeredBy = "";

        foreach ($query->result_array() as $row) {
            $answeredBy = $row['answeredBy'];
        }

        //Only the user who answered/ admin can delete an answer
        if ($answeredBy === $uid || $userGroup === ADMIN_GROUP) {


            $this->db->select('question');
            $q1 = $this->db->get_where('answer', array('id' => $id));
            $ans1 = $q1->row(0);
            $result1 = (Array) $ans1;
            $result1 = $result1['question'];

            $this->db->select('answers');
            $q2 = $this->db->get_where('question', array('id' => $result1));
            $ans2 = $q2->row(0);
            $result2 = (Array) $ans2;
            $result2 = $result2['answers'];

            $this->db->select('acceptedAnswer');
            $q0 = $this->db->get_where('question', array('id' => $result1));
            $ans0 = $q0->row(0);
            $result0 = (Array) $ans0;
            $result0 = $result0['acceptedAnswer'];

            $answers = explode(',', $result2);
            $ansId = '';

            for ($i = 0; $i < count($answers); $i++) {
                if ($i == 0) {
                    if ($answers[$i] != $id)
                        $ansId = $answers[$i];
                    else
                        $ansId = '';
                }else {
                    if ($answers[$i] != $id) {
                        if ($ansId == '')
                            $ansId = $answers[$i];
                        else
                            $ansId = $ansId . ',' . $answers[$i];
                    }
                }
            }

            $data = array('answers' => $ansId);

            $this->db->where('id', $result1);
            $res = $this->db->update('question', $data);// Update answers column of question table removing the deleted answer id

            if ($result0 === $id) {
                $data0 = array('acceptedAnswer' => '0');

                $this->db->where('id', $result1);
                $res = $this->db->update('question', $data0);//Update the acceptedAnswer column if the answer was a n accepted one
            }

            if ($res === TRUE) {
                $aId = array('id' => $id);
                $res = $this->db->delete('answer', $aId);//Delete the answer from asnwer table
            }
            return $res;
        } else if ($answeredBy !== $uid) {
            return "You can ONLY delete the answers given by you";
        }
    }

    //Edit a question
    function editQuestion($id, $title, $description) {

        if ($title === '' || $description === '')
            return "Title and description cannot be empty. If you want to delete the question, please use the 'Delete' button";

        $this->db->select('votes');
        $qr = $this->db->get_where('question', array('id' => $id));
        $votes = "";

        foreach ($qr->result_array() as $row) {
            $votes = $row['votes'];
        }

        if ($votes !== '0')// User is not allowed to edit a voted question
            return "You cannot edit a voted answer.";
        
        
        $this->db->select('answers');
        $query = $this->db->get_where('question', array('id' => $id));
        $answers = "";

        foreach ($query->result_array() as $row) {
            $answers = $row['answers'];
        }

        if ($answers !== '')//User is not allowed to edit an answered question
            return "You cannot edit an answered question.";

        $this->db->select('askedBy');
        $query = $this->db->get_where('question', array('id' => $id)); //Get the user id of the user who asked the question
        $askedBy = "";

        foreach ($query->result_array() as $row) {
            $askedBy = $row['askedBy'];
        }
        
        $uid = $this->getUserId(); //Get the currenlt logged in user
        
        if ($askedBy === $uid) { //If the logged in user is the one who asked the question he can edit it
            $data = array('title' => $title, 'description' => $description);

            $this->db->where('id', $id);
            $msg = $this->db->update('question', $data);//Update the edited question
            return $msg;
        } else {
            return "You can ONLY edit the questions asked by you";
        }
    }

    //Edit answer
    function editAnswer($id, $answer) {
        if ($answer === '')
            return "Answer cannot be empty. If you want to delete the answer, please use the 'Delete' button";

        $this->db->select('votes');
        $query = $this->db->get_where('answer', array('id' => $id));
        $votes = "";

        foreach ($query->result_array() as $row) {
            $votes = $row['votes'];
        }

        if ($votes !== '0')// User is not allowed to edit a voted answer
            return "You cannot edit a voted answer.";

        $this->db->select('answeredBy');//Get the user id of answered user
        $query = $this->db->get_where('answer', array('id' => $id));
        $answeredBy = "";

        foreach ($query->result_array() as $row) {
            $answeredBy = $row['answeredBy'];
        }
        $uid = $this->getUserId();//Get the user id of logged in user

        if ($answeredBy === $uid) { //Only the logged in user who answered can edit the answer
            $data = array('answer' => $answer);

            $this->db->where('id', $id);
            $msg = $this->db->update('answer', $data);//Update the edited answer
            return $msg;
        } else {
            return "You can ONLY edit the answers given by you";
        }
    }

    //Get the user id of logged in user
    function getUserId() {
        $user = $this->authlib->is_loggedin();
        $id = $this->UserModel->getUserID($user);
        $result = (Array) $id;

        if ($user == false) {
            return false;
        } else {
            return $result['id'];
        }
    }

    //Get the total number of questions
    function getNumofQuestions() {
        return $this->db->count_all('question');
    }

    //Close a question
    function closeQuestion($id) {
        $this->db->select('askedBy');
        $q = $this->db->get_where('question', array('id' => $id));//Get the user id of the user who asked the question
        $askedBy = $q->row(0);
        $askedBy = (Array) $askedBy;
        $askedBy = $askedBy['askedBy'];

        $uid = $this->getUserId(); //Get the user id of currently logged in user

        if ($askedBy === $uid) { //Only the user who asked can close it and he must log in first
            $closeQuestion = 1;
            $data = array('isclosed' => $closeQuestion);

            $this->db->where('id', $id);
            if ($this->db->update('question', $data)) //Update the question as a closed question
                return true;
        } else {
            return "You can ONLY close the questions asked by you";
        }
    }

    //Accept an answer
    function acceptAnswer($id, $ansid) {
        $this->db->select('askedBy');
        $q = $this->db->get_where('question', array('id' => $id));//Get the id of the user who answered the question
        $askedBy = $q->row(0);
        $askedBy = (Array) $askedBy;
        $askedBy = $askedBy['askedBy'];

        $uid = $this->getUserId();//Get the id of logged in user

        if ($askedBy === $uid) {//Only the user who asked can accept an answer and he must log in first

            $querry = $this->db->get_where('question', array('id' => $id));
            $acceptedAns = $querry->row(0);
            $acceptedAns = (Array) $acceptedAns;
            $acceptedAns = $acceptedAns['acceptedAnswer'];//Get the value of acceptedAnswer column

            if ($acceptedAns !== '0') //If the user has already accepted an answer
                return "You have already accpeted an answer";

            $data = array('acceptedAnswer' => $ansid);

            $this->db->where('id', $id);
            if ($this->db->update('question', $data))//Upate the table with the accepted answer
                return "Successfully accepted the answer";
        } else {
            return "You can ONLY close the questions asked by you";
        }
    }
}

?>
