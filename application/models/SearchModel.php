<?php

class SearchModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('authlib');
    }

    //Get the total number of pages for basic search
    function numOfPage($searchTerm) {
        $keywords = explode(' ', $searchTerm);
        $tagId = $this->searchTag($searchTerm);

        foreach ($keywords as $keyword) {

            $this->db->like('title', $keyword);//Search the search term in titles fo questions
            if ($tagId !== false) {

                for ($i = 0; $i < count($tagId); $i++) {
                    $this->db->or_like('tags', $tagId[$i]);//Search the seatch term in tags
                }
            }
        }
        $res = $this->db->get('question');
        $count = $res->num_rows();//Get the number of rows
        $numofPages = (int) ($count / QUESTIONS_PER_PAGE); //Get the number of pages
        if ($count % QUESTIONS_PER_PAGE != 0)
            $numofPages = $numofPages + 1;
        return $numofPages; //Return the number of pages
    }

    //Basic search of the system
    public function basicSearch($searchTerm, $pageNumber) {

        $numOfPage = $this->numOfPage($searchTerm); //Get the number of pages
        $tagId = $this->searchTag($searchTerm); //Get the names of tages

        if ($numOfPage < 1) { // should be only ONE matching row
            return false;
        } else {
            $keywords = explode(' ', $searchTerm);
            $startingItem = ($pageNumber - 1) * QUESTIONS_PER_PAGE;
            $pageNumber = (int) $pageNumber;

            foreach ($keywords as $keyword) {
                $this->db->or_like('title', $keyword); //Search the key words in titles

                if ($tagId !== false) {

                    for ($i = 0; $i < count($tagId); $i++) { //Search the keywords in tags
                        $this->db->or_like('tags', $tagId[$i]); 
                    }
                }
            }

            $this->db->order_by('votes', 'desc'); //Order the search results by votes, descending
            $res = $this->db->get('question', QUESTIONS_PER_PAGE, $startingItem); 
            $row = array();

            for ($j = 0; $j < $res->num_rows(); $j++) {
                $row[$j] = $res->row($j);
            }
            return $row;//Return the questions
        }
    }

    //Get the number of pages for advanced search
    function numOfPageAd($searchTerms, $returnType) {
        $keywordsT = explode(' ', $searchTerms['searchTermTitle']);
        $keywordsD = explode(' ', $searchTerms['searchTermDesc']);

        $pageNumber = (int) $searchTerms['pageNumber'];
        $startingItem = ($pageNumber - 1) * QUESTIONS_PER_PAGE; //calculate the starting page

        if ($searchTerms['tag'] != '')
            $tagId = $this->searchTag($searchTerms['tag']);
        if ($searchTerms['askedBy'] != '')
            $usersIds = $this->searchAskedBy($searchTerms['askedBy']);

        foreach ($keywordsT as $keywordT) {
            if ($searchTerms['searchTermTitle'] != '') {
                $this->db->like('title', $keywordT); //Search in titles
            }
        }
        foreach ($keywordsD as $keywordD) {
            if ($searchTerms['searchTermDesc'] != '') {
                $this->db->like('description', $keywordD); //Search in description
            }
        }
        if ($searchTerms['subject'] != '0')
            $this->db->where('subject', $searchTerms['subject']); //Search in subjects

        if ($searchTerms['tag'] != '') {
            if ($tagId !== false) {

                for ($i = 0; $i < count($tagId); $i++)
                    $this->db->like('tags', $tagId[$i]); //Search in tags
            }
        }

        if ($searchTerms['askedBy'] != '') {
            $this->db->where_in('askedBy', $usersIds); // Search for the asked by user
        }
        
        if($searchTerms['type'] == 'ans'){ //Check for answered and unanswered users
            $this->db->like('answers', ',');
        }else if($searchTerms['type'] == 'unans'){
            $this->db->where_in('answers', '');
        }
        
        $this->db->order_by($searchTerms['sortBy'], $searchTerms['ordertBy']); //Sort the results

        if ($returnType == 1) {
            $res = $this->db->get('question', QUESTIONS_PER_PAGE, $startingItem);
            return $res; //Return results
        } else {
            $res = $this->db->get('question');

            $count = $res->num_rows(); //Get the number of results

            $numofPages = (int) ($count / QUESTIONS_PER_PAGE);
            if ($count % QUESTIONS_PER_PAGE != 0)
                $numofPages = $numofPages + 1;
            return $numofPages; //Return the number of pages
        }
    }

    //Advanced search of the system
    public function advanceSearch($searchTerms) {

        //Get the total number of pages
        $numOfPage = $this->numOfPageAd($searchTerms, RETURN_NUM_OF_PAGES);

        if ($numOfPage < 1) {
            return false;
        } else {
            $row = array();
            $res = $this->numOfPageAd($searchTerms, RETURN_RESULTS);
            for ($j = 0; $j < $res->num_rows(); $j++) {
                $row[$j] = $res->row($j);
            }
            return $row;//Return the search results
        }
    }

    //Number of pages for users/ questions for subjects and user profile page
    function numOfPageSpecific($label, $term, $returnType, $pageNumber) {

        if (strpos($label, 'user') !== false) { //Search for users
            $keywords = explode(' ', $term);
            $temp = array();
            $temp = str_split($label, 4); //Check whether whether the user is a student/ tutor
            $table = $temp[0];
            $userGroup = $temp[1];

            $this->db->select('id,name, favouriteSubject, joinedDate, reputation');
            foreach ($keywords as $keyword) {
                $this->db->like('name', $keyword);
            }
            $this->db->where('userGroup', $userGroup);
            $this->db->order_by('reputation', 'desc'); //Order the users by reputation
            $itemsPerPage = USERS_PER_PAGE;
        } else if ($label == 'question') { //Get the questions asked by the logged in user

            if ($term == 'askedBy') {
                $userid = $this->authlib->getloggedUserID();
                $this->db->where('askedBy', $userid);
                $this->db->order_by('askedDate', 'desc');
                $itemsPerPage = QUESTIONS_PER_PAGE;
                $table = $label;
            }
        } else if ($label == 'subject') { //Load the questions for subjects page
            $this->db->where('subject', $term);
            $this->db->order_by('askedDate', 'desc');
            $itemsPerPage = QUESTIONS_PER_PAGE;
            $table = 'question';
        }

        $startingItem = ($pageNumber - 1) * $itemsPerPage;

        if ($returnType == RETURN_RESULTS) {
            $res = $this->db->get($table, $itemsPerPage, $startingItem);
            return $res;
        } else {
            $res = $this->db->get($table);
            $count = $res->num_rows(); //Get the total number of results

            $numofPages = (int) ($count / $itemsPerPage);
            if ($count % $itemsPerPage != 0)
                $numofPages = $numofPages + 1;
            return $numofPages; //Return the number of pages
        }
    }

    //Search for users/ questions for subjects and user profile page
    public function SearchSpecific($label, $term, $pageNumber) {

        //Get the number of pages
        $numOfPage = $this->numOfPageSpecific($label, $term, RETURN_NUM_OF_PAGES, $pageNumber);

        if ($numOfPage < 1) {
            return false;
        } else {
            $row = array();
            $res = $this->numOfPageSpecific($label, $term, RETURN_RESULTS, $pageNumber);
            for ($j = 0; $j < $res->num_rows(); $j++) {
                $row[$j] = $res->row($j);
            }
            return $row; //Return the results
        }
    }

    //Get the ids of the tages according tp name
    public function searchTag($searchTerm) {
        $keywords = explode(' ', $searchTerm);
        $this->db->select('id');
        foreach ($keywords as $keyword) {
            $this->db->or_where('name', $keyword); //Search for tag ids according to names
        }
        $q = $this->db->get('tag');

        if ($q->num_rows() > 0) {
            $tagIds = array();
            $count = 0;
            foreach ($q->result_array() as $row) {

                $tagIds[$count] = $row['id'];
                $count++;
            }
            return $tagIds; //Return tag ids
        }
        return false;
    }

    //Search the user ids according to names
    public function searchAskedBy($searchTerm) {
        $keywords = explode(' ', $searchTerm);
        $this->db->select('id');
        foreach ($keywords as $keyword) {
            $this->db->or_like('name', $keyword); //Get the user ids
        }
        $q = $this->db->get('user');

        if ($q->num_rows() > 0) {
            $usersIds = array();
            $count = 0;
            foreach ($q->result_array() as $row) {

                $usersIds[$count] = $row['id'];
                $count++;
            }
            return $usersIds; //Return the user ids
        }
        return false;
    }

}

?>
