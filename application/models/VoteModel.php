<?php

class VoteModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    //Vote for a question/ answer
    public function vote($args, $id, $votes) {
        $table = $args['rest'];
        $numOfVotes = 0;       

        $this->db->select('votes');
        $query = $this->db->get_where($table, array('id' => $id));

        foreach ($query->result_array() as $row) {
            $numOfVotes = $row['votes'];
        }

        $votes = $numOfVotes + $votes; //Increase/ decrese the number of votes
        $data = array('votes' => $votes);

        $this->db->where('id', $id);
        $this->db->update($table, $data); //Record the vote
        return TRUE;       
    }
}

?>
