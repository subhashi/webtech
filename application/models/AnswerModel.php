<?php

class AnswerModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    //Answer to a question
    function answer($uid,$qid, $answer) {
                
        $answeredDate = date("Y-m-d H:i:s");
        $data = array('answer' => $answer, 'question' => $qid, 'answeredBy' => $uid, 'answeredDate' => $answeredDate);
        $this->db->insert('answer', $data);
        $newId = $this->db->insert_id();
        return $newId; 
    }
    
    //Get the total number of answers
    function getNumofAns(){
        return $this->db->count_all('answer');
    }

}

?>
